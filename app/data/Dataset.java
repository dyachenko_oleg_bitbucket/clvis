package app.data;


import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

import org.jdom2.Element;

import app.graph.Node;
import app.graph.Relation;
import app.graph.TreeNode;
import app.gui.AbstractModel;
import app.gui.Table;
import app.visualizer.Visualizer;
import app.visualizer.colorers.EdgeColorer;
import app.visualizer.colorers.VertexColorer;
import app.xml.XMLManager;


public class Dataset implements DatasetInterface {
	public String path; 
	
	private VertexColorer vertexColorer = new VertexColorer();
	private EdgeColorer edgeColorer = new EdgeColorer();
	
	public VertexColorer getVertexColorer() {
		return vertexColorer;
	}
	public EdgeColorer getEdgeColorer() {
		return edgeColorer;
	}
	
	
	
	
	private String focusVertex = "";
	public String getFocus() {
		return focusVertex;
	}
	public String setFocus(String focus) {
		focusVertex = focus;
		return getFocus();
	}
		
	
	/* XML */
	XMLManager xml = null;
	
	public XMLManager getXML() {
		return null;
	}
	
	/* Nodes-Relations-TreeNodes */
	Map<String, Node> nodes = null;
	Map<String, Relation> relations = null;
	Map<String, TreeNode> treeNodes = null;

	@Override
	public void init() {
		// Override in subclasses
	}

	@Override
	public Map<String, Node> getNodes() {
		return nodes;
	}

	@Override
	public Map<String, Relation> getRelations() {
		return relations;
	}

	@Override
	public Map<String, TreeNode> getTreeNodes() {
		return treeNodes;
	}
	
	public void clean() {
		for( String key : getTreeNodes().keySet() ) {
			getTreeNodes().get(key).flush();
		}
	}
	
	/* Filters */
	public static final int ENTITY = 0;
	public static final int OBJECT = 1;
	public static final int RELATION = 2;
	
	Map<String,TreeNodeState> f_entities = null;
	Map<String,TreeNodeState> f_objects = null;
	Map<String,TreeNodeState> f_relations = null;
	
	private ArrayList<String> getIds( int code ) {
		ArrayList<String> list = new ArrayList<String>();
		if( listByCode(code) != null ) {
			list.addAll(listByCode(code).keySet());	
		}
		for( int i=0; i<list.size(); i++ ) {
			for( int j=0; j<i; j++ ) {
				//if( Integer.parseInt(list.get(i)) < Integer.parseInt(list.get(j)) ) {
				if( list.get(i).compareTo(list.get(j)) < 0 ) {
					String swap = list.get(i);
					list.set(i, list.get(j));
					list.set(j, swap);
				}
			}
		}
		return list;
	}
	public ArrayList<String[]> getOrderedList( int code ) {
		ArrayList<String> list = getIds(code);
		String name = "";
		ArrayList<String[]> list2 = new ArrayList<String[]>();
		for( String s : list ) {
			switch(code) {
			case OBJECT: name = getObjectName(s); break;
			case ENTITY: name = getEntityName(s); break;
			case RELATION: name = getRelationName(s); break;
			}
			list2.add( new String[]{s,name} );
		}
		for( int i=0; i<list2.size(); i++ ) {
			for( int j=0; j<i; j++ ) {
				if( list2.get(i)[1].compareTo(list2.get(j)[1]) < 0 ) {
					String[] swap = list2.get(i);
					list2.set(i, list2.get(j));
					list2.set(j, swap);
				}
			}
		}
		return list2;
	}
	
	public ArrayList<String> getEntitiesIds() {
		return getIds(ENTITY);
	}
	public ArrayList<String> getObjectIds() {
		return getIds(OBJECT);
	}
	public ArrayList<String> getRelationIds() {
		return getIds(RELATION);
	}
	
	
	protected Map<String,TreeNodeState> listByCode(int code) {
		switch(code) {
			case ENTITY: return f_entities;
			case OBJECT: return f_objects;
			case RELATION: return f_relations;
		}
		return f_entities;
	}
	
	public void off( String key, int code ){
		listByCode(code).put(key, TreeNodeState.OFF);
	}
	public void hide( String key, int code ){
		listByCode(code).put(key, TreeNodeState.HIDE);
	}
	public void show( String key, int code ){
		listByCode(code).put(key, TreeNodeState.SHOW);
	}
	public TreeNodeState status( String key, int code ){
		if( listByCode(code).containsKey(key) ) {
			return listByCode(code).get(key); 
		} else {
			return TreeNodeState.OFF;
		}
	}
	public boolean getVertexStatus( TreeNode root ){
		return true;
	}
	@Override
	public boolean getEdgeStatus(Relation e) {
		return true;
	}
	
	@Override
	public void initFilters() {
		// Override in subclasses
	}
	
	class FilterPopUp extends JPopupMenu {
	    /**
		 * 
		 */
		private static final long serialVersionUID = 2835640534180271912L;
		JMenuItem show;
	    JMenuItem hide;
	    JMenuItem off;
	    public FilterPopUp(Table table, int code){
	        show = new JMenuItem("Показать");
	    	hide = new JMenuItem("Скрыть");
	    	off  = new JMenuItem("Удалить");
	    	
	    	final int code2 = code; 
	    	final Table table2 = table;
	    	show.addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					setFilterValues(table2, 2, code2);
				}
	    	});
	    	hide.addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					setFilterValues(table2, 1, code2);
				}
	    	});
	    	off.addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					setFilterValues(table2, 0, code2);
				}
	    	});
	    	
	    	add(show);
	    	add(hide);
	    	add(off);
	    }
	    
	    private void setFilterValues(Table table2, int val, int code2) {
	    	int[] rows = table2.getSelectedRows();
			for( int row : rows ) {
				table2.setValueAt(
					TreeNodeState.values()[val].toString(), 
					row, 2
				);
				listByCode(code2).put(row+"",  
					TreeNodeState.values()[val]
				);
			}
			table2.repaint();
			Visualizer.vv.repaint();
	    }
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void openFilterDialog(int code) throws Exception {
		JDialog dialog = new JDialog();
		dialog.setTitle("Задайте фильтры");
		dialog.getContentPane().setLayout(new BorderLayout());
		
		String title = "";
		switch(code) {
		case OBJECT: title = "Имя объекта"; break;
		case ENTITY: title = "Имя сущности"; break;
		case RELATION: title = "Отношение"; break;
		}
		final Table table = new Table(new AbstractModel(
			new Vector<String>(Arrays.asList(new String[]{ "ID", title, "Вкл/Выкл" })),
			new Vector<Class>(Arrays.asList(new Class[]{ String.class, String.class, String.class }))
		));
		table.setColWidth(0, 80);
		table.setColWidth(2, 60);
		//table.addRenderer(1);
		table.setRowSelectionAllowed(true);
		
		/*
		ArrayList<String> ids = new ArrayList<String>();
		switch(code) {
		case OBJECT: ids = getObjectIds(); break;
		case ENTITY: ids = getEntitiesIds(); break;
		case RELATION: ids = getRelationIds(); break;
		}
		*/
		
		ArrayList<String[]> ids = getOrderedList( code );
		for( String[] s : ids ) {
			if(s[1].equals("")) continue;
			Vector<Object> v = new Vector<Object>();
			v.add(s[0]);
			v.add(s[1]);
			v.add(status(s[0], code).toString());
			table.addRow(v);
		}
		
		final int code2 = code;
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Point pt = table.getSelectedCell();
				if (e.getButton() == 3) {
					filterPopUp(e, code2);	
					return;
				}
				if( pt.x >= 0 ) {
					TreeNodeState tns = TreeNodeState.fromString( (String)table.getValueAt(pt.x, 2) );
					table.setValueAt(
						tns.next().toString(), 
						pt.x, 2
					);
					listByCode(code2).put(
						(String)table.getValueAt(pt.x, 0), 
						tns.next()
					);
					table.repaint();
					Visualizer.vv.repaint();
				}
			}
			
			private void filterPopUp(MouseEvent e, int code){
		        FilterPopUp menu = new FilterPopUp(table, code);
		        menu.show(e.getComponent(), e.getX(), e.getY());
		    }
		});
		
		JScrollPane jsp = new JScrollPane(table);
		dialog.getContentPane().add(BorderLayout.CENTER, jsp);
		
		dialog.setSize( 600,400 );
		dialog.setModal(true);
		dialog.setVisible(true);
	}
	
	/* Filtered XML */
	XMLManager xmlf = null;
	@Override
	public XMLManager getXMLf() {
		return xmlf;
	}
	
	protected void filterByIds( ArrayList<Element> elems, ArrayList<String> ids, String name ) {
		for( int i=elems.size()-1; i>=0; i-- ) {
			if( !ids.contains(elems.get(i).getAttributeValue(name)) ) {
				elems.remove(i);
			}
		}
	}
	protected void getIds( ArrayList<Element> elems, ArrayList<String> ids, String name ) {
		for( Element e : elems ) {
			if( !ids.contains(e.getAttributeValue(name)) ) {
				ids.add(e.getAttributeValue(name));
			}
		}
	}
	
	@Override
	public void applyFilters(String path) {
		// Override in subclasses
	}
	
	@Override
	public ArrayList<TreeNode> getConnectedComponents(String focusNode) {
		ArrayList<TreeNode> components = new ArrayList<TreeNode>();
		ArrayList<String> ids = new ArrayList<String>();
		for( Element e : getXMLf().search("objects", "object") ) {
			ids.add(e.getAttributeValue("id"));
		}
 
		Visualizer.treeNodes.clear();
		for( String id : ids ) {
			Visualizer.treeNodes.put(id, getTreeNodes().get(id));
		}
		clean();
		while( ids.size() != 0 ) {
			TreeNode tn = getConnectedComponent(focusNode, ids);
			tn.setLevels();
			components.add(tn);
		}
		for( int i=0; i<components.size(); i++ ) {
			for(int j=0; j<i; j++ ) {
				if( components.get(i).countElements() > components.get(j).countElements() ) {
					TreeNode swap = components.get(i);
					components.set(i, components.get(j));
					components.set(j, swap);
				}
			}
		}
		/*
		for( int i=components.size()-1; i>=0; i-- ) {
			if( components.get(i).countElements() < 2 ) {
				off( components.get(i).getId(), OBJECT );
				components.remove(i);
			}
		}
		*/
		return components;
	}

	public TreeNode getConnectedComponent(String focusNode, ArrayList<String> ids) {
		// Override in subclass
		return null;
	}

	
	/* INFO containers */
	protected Map<TreeNode,String> vertexLabels = new HashMap<TreeNode, String>();
	protected Map<TreeNode,String> vertexNames = new HashMap<TreeNode, String>();
	protected Map<TreeNode,String> vertexFullNames = new HashMap<TreeNode, String>();
	protected Map<TreeNode,String> vertexInfos = new HashMap<TreeNode, String>();
	protected Map<Relation,String> edgeLabels = new HashMap<Relation, String>();
	protected Map<Relation,String> edgeNames = new HashMap<Relation, String>();
	protected Map<Relation,String> edgeInfos = new HashMap<Relation, String>();
	
	/* INFO: vertex */
	public String getVertexLabel( TreeNode root ) {
		return "";
	};
	public String getVertexName( TreeNode root ) {
		return "";
	};
	public String getVertexFullName( TreeNode root ) {
		return "";
	};
	public String getVertexInfo( TreeNode root ) {
		return "";
	};
	
	/* INFO: edge */
	public String getEdgeLabel( Relation rel ) {
		return "";
	};
	public String getEdgeName( Relation rel ) {
		return "";
	};
	public String getEdgeInfo( Relation rel ) {
		return "";
	};
	
	/* INFO: data */
	public String getEntityName( String id ) {
		return "Entity #"+id;
	};
	public String getObjectName( String id ) {
		return "Object #"+id;
	};
	public String getRelationName( String id ) {
		return "Relation #"+id;
	};
	
	
	/* Info */
	/*
	
	protected String getScheme( String entityId, boolean shortScheme ) {
		ArrayList<Element> res = getXML().search("entities", "entity", new String[]{"id",entityId});
		if( !res.isEmpty() ) {
			return res.get(0).getAttributeValue(shortScheme ? "name_s" : "name_f");
		}
		return null;
	}
	
	@Override
	public String getVertexLabel(TreeNode root) {
		return root.getName(false);
	}
	@Override
	public String getVertexShortLabel(TreeNode root) {
		return root.getName(true);
	}
	
	
	
	@Override
	public String getVertexInfo(TreeNode root) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getEdgeLabel(Relation rel) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getEdgeInfo(Relation rel) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public String getEntityName( String id ) {
		return "";
	}
	public String getRelationName( String id ) {
		return "";
	}
	*/
	
}
