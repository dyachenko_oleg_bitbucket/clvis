package app.data;


import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import app.config.Configuration;
import app.graph.Node;
import app.graph.Relation;
import app.graph.RelationPair;
import app.graph.TreeNode;
import app.xml.XMLManager;

public class DatasetData extends Dataset {
	@Override
	public XMLManager getXML() {
		return Configuration.xml_data;
	}
	
	@Override
	public void init() {
		if( nodes == null ) {
			nodes = new HashMap<String, Node>();
			ArrayList<Element> res = getXML().search("objects", "object");
			for( Element e : res ) {
				nodes.put(
					e.getAttributeValue("id"),
					new Node(e.getAttributeValue("id"),e.getAttributeValue("id_ent"))
				);
			}
		}
		if( relations == null ) {
			relations = new HashMap<String, Relation>();
			ArrayList<Element> res = getXML().search("relations", "relation");
			for( Element e : res ) {
				relations.put(
					e.getAttributeValue("id"),
					new Relation(e.getAttributeValue("id"), e.getAttributeValue("id_entity"))
				);
			}
		}
		if( treeNodes == null ) {
			treeNodes = new HashMap<String, TreeNode>();
			for( String id : nodes.keySet() ) {
				treeNodes.put(id, new TreeNode(getNodes().get(id)));
			}
		}
	}
	
	@Override
	public void initFilters() {
		ArrayList<Element> res;
		if( f_entities == null ) {
			f_objects = new HashMap<String, TreeNodeState>();
			f_entities = new HashMap<String, TreeNodeState>();
			f_relations = new HashMap<String, TreeNodeState>();
			res = getXML().search("objects", "object");
			for( Element e : res ) {
				show( e.getAttributeValue("id"), OBJECT );
			}
			res = getXML().search("entities", "entity", new String[]{ "type", "1" });
			for( Element e : res ) {
				show( e.getAttributeValue("id"), ENTITY );
			}
			res = getXML().search("entities", "entity", new String[]{});
			for( Element e : res ) {
//				if( !"1".equals(e.getAttributeValue("type")) ) {
//					if( 
//							"47".equals(e.getAttributeValue("id")) || 
//							"48".equals(e.getAttributeValue("id")) || 
//							"50".equals(e.getAttributeValue("id"))
//					) 
						show( e.getAttributeValue("id"), RELATION );
//					else off( e.getAttributeValue("id"), RELATION );
//				}
			}
			
			// filters from configs
			// TODO
//			String[] disabled;
//			disabled = Configuration.get("dataDisabledObjects").split(",");
//			for( String key : disabled ) {
//				if( !key.equals("") ) off(key, OBJECT);
//			}
//			disabled = Configuration.get("dataDisabledEntities").split(",");
//			for( String key : disabled ) {
//				if( !key.equals("") ) off(key, ENTITY);
//			}
//			disabled = Configuration.get("dataDisabledRelations").split(",");
//			for( String key : disabled ) {
//				if( !key.equals("") ) off(key, RELATION);
//			}
		}
	}
	
	
	
	@Override
	public void applyFilters(String path) {
		this.path = path;
		ArrayList<Element> ent = getXML().search("entities", "entity", new String[]{"type","1"});
		ArrayList<Element> obj = getXML().search("objects", "object");
		ArrayList<Element> rel = getXML().search("relations", "relation");
		
		ArrayList<String> ids_e = new ArrayList<String>();
		ArrayList<String> ids_o = new ArrayList<String>();
		ArrayList<String> ids_r = new ArrayList<String>();
		for( String key : listByCode(ENTITY).keySet() ) {
			if( !listByCode(ENTITY).get(key).isOFF() ) {
				ids_e.add(key);
			}
		}
		for( String key : listByCode(OBJECT).keySet() ) {
			if( !listByCode(OBJECT).get(key).isOFF() ) {
				ids_o.add(key);
			}
		}
		for( String key : listByCode(RELATION).keySet() ) {
			if( !listByCode(RELATION).get(key).isOFF() ) {
				ids_r.add(key);
			}
		}
		
		filterByIds( ent, ids_e, "id" );
		filterByIds( obj, ids_e, "id_ent" );
		filterByIds( obj, ids_o, "id" );
		filterByIds( rel, ids_r, "id_entity" );

		for( int i=rel.size()-1; i>=0; i-- ) {
			if( 
				!ids_o.contains(rel.get(i).getAttributeValue("from")) ||
				!ids_o.contains(rel.get(i).getAttributeValue("to"))
			) { rel.remove(i); }
		}
		
		// put it to the datafile
		Element root = new Element("data");
		Document doc = new Document(root);
		
		Element root_obj = null;
		root_obj = new Element("objects");
		for( Element e : obj ) {
			root_obj.addContent((Element)e.clone());
		}
		doc.getRootElement().addContent(root_obj);
		
		root_obj = new Element("relations");
		for( Element e : rel ) {
			root_obj.addContent((Element)e.clone());
		}
		doc.getRootElement().addContent(root_obj);
		
		root_obj = new Element("entities");
		for( Element e : ent ) {
			root_obj.addContent((Element)e.clone());
		}
		doc.getRootElement().addContent(root_obj);	
		
//		XMLOutputter xmlOutput = new XMLOutputter();
//		xmlOutput.setFormat(Format.getPrettyFormat());
//		try {
//			xmlOutput.output(doc, new FileWriter(path));
//		} catch (IOException e1) {
//			System.out.println(path);
//			e1.printStackTrace();
//		}
		
//		xmlf = new XMLManager(path);
		xmlf = new XMLManager(doc.getRootElement());
	}	
	
	
	
	public boolean getVertexStatus( TreeNode root ){
		return status(root.getId(), Dataset.OBJECT).isShown() &&
				status(root.getEntity(), Dataset.ENTITY).isShown();
	}
	public boolean getEdgeStatus(Relation e) {
		return listByCode(RELATION).containsKey(e.getId()) ? listByCode(RELATION).get(e.getId()).isShown() : true;
	}
	
	@Override
	public TreeNode getConnectedComponent(String focusNode, ArrayList<String> ids) {
		String focus = ids.contains(focusNode) ? focusNode : ids.get(0);
		TreeNode root = getTreeNodes().get(focus);
		root.setParent(null);
		
		ArrayList<String> treeNodeIds = new ArrayList<String>();
		ArrayList<String> wave = new ArrayList<String>();
		
		wave.add(focus);
		treeNodeIds.add(focus);
		
		ArrayList<Element> res = null;
		String[] fromto = new String[]{ "to", "from" } ;
		
		int k = 0;
		while( wave.size() !=0 ) {
			ArrayList<String> newWave = new ArrayList<String>();
			for( String id : wave ) {
				for(int i=Configuration.get("directEdges").equals("1")?1:0; i<=1; i++) {
					res = getXMLf().search(
						"relations", "relation", 
						XMLManager.createHashMap(new String[]{fromto[1-i],id})
					);
					for( Element e : res ) {
						String val = e.getAttributeValue(fromto[i]);
						if( treeNodeIds.contains(val) ) {
							//System.out.println(id+" : "+val);
							getTreeNodes().get(id).addChildren(
								getTreeNodes().get(val), 
								new RelationPair(getRelations().get(e.getAttributeValue("id")), i!=0)
							);
						} else if( ids.contains(val) ) {
							//System.out.println(id+" : "+val);
							newWave.add(val);
							treeNodeIds.add(val);
							getTreeNodes().get(id).addChildren(
								getTreeNodes().get(val), 
								new RelationPair(getRelations().get(e.getAttributeValue("id")), i!=0)
							);
							
						}
					}
				}
			}
			wave = newWave;
			//k++; if( k == 3 ) break;
		}
		for( String id : treeNodeIds ) {
			ids.remove(id);
		}
		return root;
	}
	
	/* INFO: vertex */
	public String getVertexLabel( TreeNode root ) {
		return getVertexName(root);
	};
	
	private String[] getScheme( String id_ent, String paramName ) {
		ArrayList<Element> res = getXML().search("entities", "entity", new String[]{"id",id_ent});
		if( !res.isEmpty() ) {
			String s = res.get(0).getAttributeValue(paramName);
			if( s != null) {
				return s.split(" ");
			}
		}
		return new String[]{};
	} 
	private String getVertexName( String id, String paramName ) {
		ArrayList<Element> res = getXML().search("objects", "object", new String[]{"id",id});
		if( res.isEmpty() ) return ""; 
		Element element = res.get(0);
		String[] scheme = getScheme(element.getAttributeValue("id_ent"), paramName);
		String name = "";
		for( String s : scheme ) {
			char code = s.charAt(0);
			String _id = s.substring(1);
			String _name = "";
			if( code == 'a' ) {
				for(Object attr : element.getChildren()) {
					if( _id.equals( ((Element)attr).getAttributeValue("id")) ) {
						String value = ((Element)attr).getAttributeValue("value");
						if( value != null && !value.equals("") ) {
							if( !_name.equals("") ) {
								_name += " ";
							} 
							_name += value;
						}
					}
				}
				
			} else if( code == 'r' ) {
				ArrayList<Element> tmp;
				String[] fromto = new String[]{"from","to"};
				for( int i=0; i<=1; i++ ) {
					tmp = getXML().search("relations", "relation", new String[]{"id_entity",_id,fromto[i],id});
					for( Element e : tmp ) {
						String value = getVertexName(e.getAttributeValue(fromto[1-i]), paramName);
						if( value != null && !value.equals("") ) {
							if( !_name.equals("") ) {
								_name += " ";
							} 
							_name += value;
						}
					}
				}
			}
			if( !_name.equals("") ) {
				if(!name.equals("")) {
					name += " ";
				}
				name += _name;
			}
		}
		return name;
	}
	public String getVertexName( TreeNode root ) {
		if( root == null ) return "";
		if( vertexNames.containsKey(root) ) {
			return vertexNames.get(root);
		}
		String s = getVertexName( root.getId(), "short_link" );
		vertexNames.put(root,s);
		return s;
	};
	public String getVertexFullName( TreeNode root ) {
		return "";
	};
	public String getVertexInfo( TreeNode root ) {
		if( root == null ) return "";
		if( vertexInfos.containsKey(root) ) {
			return vertexInfos.get(root);
		}
		String s = "<html>";
		ArrayList<Element> res = getXML().search("objects", "object", new String[]{"id",root.getId()});
		if( !res.isEmpty() ) {
			s += "<table border=1 width='300px'>";
			s += "<tr><td align='right' width='100px'><b>Класс</b></td><td>" + getEntityName(res.get(0).getAttributeValue("id_ent")) + "</td></tr>";
			s += "<tr><td align='right' width='100px'><b>id</b></td><td>" + res.get(0).getAttributeValue("id") + "</td></tr>";
			s += "</table>";
			s += "<center><b>Атрибуты</b></center>";
			s += "<table border=1 width='300px'>";
			ArrayList<String> list = new ArrayList<String>();
			for( Object el : res.get(0).getChildren() ) {
				s += "<tr><td align='right' width='100px'><b>" +
					getAttributeName(((Element)el).getAttributeValue("id"))
					+ "</b></td><td>" + ((Element)el).getAttributeValue("value") + "</td></tr>";
			}
			s += "</table>";
		}
		s += "</html>";
		vertexInfos.put(root,s);
		return s;
	};
	
	/* INFO: edge */
	public String getEdgeLabel( Relation rel ) {
		return getEdgeInfo(rel);
	};
	public String getEdgeName( Relation rel ) {
		if( rel == null ) return "";
		if( edgeNames.containsKey(rel) ) {
			return edgeNames.get(rel);
		}
		String s = "";
		ArrayList<Element> res = getXML().search("relations", "relation", new String[]{"id",rel.getId()});
		if( !res.isEmpty() ) {
			ArrayList<String> list = new ArrayList<String>();
			for( Object el : res.get(0).getChildren() ) {
				list.add(((Element)el).getAttributeValue("value"));
			}
			if( list.isEmpty() ) {
				s = "Отношение #"+res.get(0).getAttributeValue("id");
			} else {
				for(int i=0; i<list.size(); i++) {
					if(i != 0) {
						s += ", ";
					}
					s += list.get(i);
				}
			}
		}
		edgeNames.put(rel,s);
		return s;
	};
	public String getEdgeInfo( Relation rel ) {
		if( rel == null ) return "";
		if( edgeInfos.containsKey(rel) ) {
			return edgeInfos.get(rel);
		}
		String s = "<html>";
		ArrayList<Element> res = getXML().search("relations", "relation", new String[]{"id",rel.getId()});
		if( !res.isEmpty() ) {
			s += "<table border=1 width='300px'>";
			s += "<tr><td align='right' width='100px'><b>Класс</b></td><td>" + getRelationName(res.get(0).getAttributeValue("id_entity")) + "</td></tr>";
			s += "<tr><td align='right' width='100px'><b>id</b></td><td>" + res.get(0).getAttributeValue("id") + "</td></tr>";
			s += "</table>";
			s += "<center><b>Аргументы</b></center>";
			s += "<table border=1 width='300px'>";
			s += "<tr><td align='right' width='100px'><b>Первый</b></td><td>" + getVertexName(getTreeNodes().get(res.get(0).getAttributeValue("from"))) + "</td></tr>";
			s += "<tr><td align='right' width='100px'><b>Второй</b></td><td>" + getVertexName(getTreeNodes().get(res.get(0).getAttributeValue("to"))) + "</td></tr>";
			s += "</table>";
			s += "<center><b>Атрибуты</b></center>";
			s += "<table border=1 width='300px'>";
			
			ArrayList<String> list = new ArrayList<String>();
			for( Object el : res.get(0).getChildren() ) {
				s += "<tr><td align='right' width='100px'><b>" +
					getAttributeName(((Element)el).getAttributeValue("id"))
					+ "</b></td><td>" + ((Element)el).getAttributeValue("value") + "</td></tr>";
			}
			s += "</table>";
		}
		s += "</html>";
		edgeInfos.put(rel,s);
		return s;
	};
	
	/* INFO: data */
	public String getEntityName( String id ) {
		ArrayList<Element> res = getXML().search("entities", "entity", new String[]{"id",id});
		if( !res.isEmpty() ) {
			return res.get(0).getAttributeValue("name");
		}
		return "";
	};
	public String getObjectName( String id ) {
		return getVertexName(getTreeNodes().get(id));
	};
	public String getRelationName( String id ) {
		ArrayList<Element> res = getXML().search("entities", "entity", new String[]{"id",id});
		if( !res.isEmpty() ) {
			return res.get(0).getAttributeValue("name");
		}
		return "";
	};
	
	private Map<String,String> attributes = new HashMap<String, String>();
	private String getAttributeName( String id ) {
		if( attributes.containsKey(id) ) {
			return attributes.get(id);
		}
		String s = "";
		ArrayList<Element> res = getXML().search("attributes", "attribute", new String[]{"id",id});
		if( !res.isEmpty() ) {
			s = res.get(0).getAttributeValue("name");
		}
		return s;
	}
	
	/*
	public String getEntityName( String id ) {
		
	}
	
	/* Info */
	/*
	@Override
	public String getVertexLabel(TreeNode root) {
		if( super.getVertexLabel(root) != null ) {
			return super.getVertexLabel(root);
		} else {
			String name = "";
			ArrayList<Element> res = getXML().search("objects", "object", new String[]{"id",root.getId()});
			if( !res.isEmpty() ) {
				if( res.get(0).getAttributeValue("name") != null ) { // has name xml attribute
					name = res.get(0).getAttributeValue("name");
				} else {
					String scheme = getScheme( res.get(0).getAttributeValue("id_ent") , false);
					if( scheme != null ) {
						String[] sname = scheme.split(" ");
						for( int i=0; i<sname.length; i++ ) {
							char code = sname[i].charAt(0);
							String id = sname[i].substring(1);
							sname[i] = "";
							if( code == 'o' ) {
								sname[i] = getVertexShortLabel(getTreeNodes().get( id ));
							} else if( code == 'a' ) {
								for( Object elem : res.get(0).getChildren() ) {
									if( ((Element)elem).getAttributeValue("id").equals(id) ) {
										sname[i] = ((Element)elem).getAttributeValue("value");
										break;
									}
								}
							} else if( code == 'r' ) {
								ArrayList<Element> tmp;
								tmp = getXML().search("relations", "relation", new String[]{"id_entity",id,"from",root.getId()});
								if(!tmp.isEmpty()) {
									sname[i] = getVertexShortLabel(getTreeNodes().get( tmp.get(0).getAttributeValue("to") ));
								}
								tmp = getXML().search("relations", "relation", new String[]{"id_entity",id,"to",root.getId()});
								if(!tmp.isEmpty()) {
									sname[i] = getVertexShortLabel(getTreeNodes().get( tmp.get(0).getAttributeValue("from") ));
								}
							}
						}
						for( int i=0; i<sname.length; i++ ) {
							if( !sname[i].equals("") ) {
								name += (i==0?"":" ") + sname[i];
							}
						}
					}
				}
			}
			if( name.equals("") ) {
				name = "Object #"+root.getId();
			}
			return root.setName(name);
		}	
	}
	@Override
	public String getVertexShortLabel(TreeNode root) {
		if( super.getVertexShortLabel(root) != null ) {
			return super.getVertexShortLabel(root);
		} else {
			String name = "";
			ArrayList<Element> res = getXML().search("objects", "object", new String[]{"id",root.getId()});
			if( !res.isEmpty() ) {
				if( res.get(0).getAttributeValue("name") != null ) { // has name xml attribute
					name = res.get(0).getAttributeValue("name");
				} else {
					String scheme = getScheme( res.get(0).getAttributeValue("id_ent") , false);
					if( scheme != null ) {
						String[] sname = scheme.split(" ");
						for( int i=0; i<sname.length; i++ ) {
							char code = sname[i].charAt(0);
							String id = sname[i].substring(1);
							sname[i] = "";
							if( code == 'o' ) {
								sname[i] = getVertexShortLabel(getTreeNodes().get( id ));
							} else if( code == 'a' ) {
								for( Object elem : res.get(0).getChildren() ) {
									if( ((Element)elem).getAttributeValue("id").equals(id) ) {
										sname[i] = ((Element)elem).getAttributeValue("value");
										break;
									}
								}
							} else if( code == 'r' ) {
								ArrayList<Element> tmp;
								tmp = getXML().search("relations", "relation", new String[]{"id_entity",id,"from",id});
								if(!tmp.isEmpty()) {
									sname[i] = getVertexShortLabel(getTreeNodes().get( tmp.get(0).getAttributeValue("to") ));
								}
								tmp = getXML().search("relations", "relation", new String[]{"id_entity",id,"to",id});
								if(!tmp.isEmpty()) {
									sname[i] = getVertexShortLabel(getTreeNodes().get( tmp.get(0).getAttributeValue("from") ));
								}
							}
						}
						for( int i=0; i<sname.length; i++ ) {
							if( !sname[i].equals("") ) {
								name += (i==0?"":" ") + sname[i];
							}
						}
					}
				}
			}
			if( name.equals("") ) {
				name = "Object #"+root.getId();
			}
			return root.setName(name);
		}
	}
	
	
	/*
	public Table colorTable() {
		final Table table = new Table(new AbstractModel(
			new Vector<String>(Arrays.asList(new String[]{
				"ID сущности", "Имя сущности", "Цвет" 	
			})),
			new Vector<Class>(Arrays.asList(new Class[]{
				String.class,
				String.class,
				Color.class
			}))
		));
		
	}
	*/
	
}
