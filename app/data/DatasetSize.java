package app.data;


import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import app.config.Configuration;
import app.graph.Node;
import app.graph.Relation;
import app.graph.RelationPair;
import app.graph.TreeNode;
import app.visualizer.Visualizer;
import app.xml.XMLManager;


public class DatasetSize extends Dataset {
	@Override
	public void init() {
		if( nodes == null ) {
			nodes = new HashMap<String, Node>();
			ArrayList<Element> res = getXML().search("entities", "entity");
			for( Element e : res ) {
				nodes.put(
					e.getAttributeValue("id"),
					new Node(e.getAttributeValue("id"), "")
				);
			}
		}
		if( relations == null ) {
			relations = new HashMap<String, Relation>();
			ArrayList<Element> res = getXML().search("relations", "relation");
			for( Element e : res ) {
				relations.put(
					e.getAttributeValue("id"),
					new Relation(e.getAttributeValue("id"))
				);
			}
		}
		if( treeNodes == null ) {
			treeNodes = new HashMap<String, TreeNode>();
			for( String id : nodes.keySet() ) {
				treeNodes.put(id, new TreeNode(getNodes().get(id)));
			}
		}
	}
	
	@Override
	public void initFilters() {
		ArrayList<Element> res;
		if( f_entities == null ) {
			f_objects = new HashMap<String, TreeNodeState>();
			f_entities = new HashMap<String, TreeNodeState>();
			f_relations = new HashMap<String, TreeNodeState>();
			res = getXML().search("entities", "entity");
			for( Element e : res ) {
				show( e.getAttributeValue("id"), OBJECT );
			}
			res = getXML().search("relations", "relation");
			for( Element e : res ) {
				if( "Класс-подкласс".equals(e.getAttributeValue("name")) )
					show( e.getAttributeValue("id"), RELATION );
				else off( e.getAttributeValue("id"), RELATION );
			}
			show( "", ENTITY );
			
			// filters from configs
			/*
			String[] disabled;
			disabled = Configuration.get("ontoDisabledObjects").split(",");
			for( String key : disabled ) {
				if( !key.equals("") ) off(key, OBJECT);
			}
			disabled = Configuration.get("ontoDisabledEntities").split(",");
			for( String key : disabled ) {
				if( !key.equals("") ) off(key, ENTITY);
			}
			disabled = Configuration.get("ontoDisabledRelations").split(",");
			for( String key : disabled ) {
				if( !key.equals("") ) off(key, RELATION);
			}
			*/
		}
	}
	
	@Override
	public void applyFilters(String path) {
		this.path = path;
		
		ArrayList<Element> ent = new ArrayList<Element>();
		Element entity = new Element("entity");
		entity.setAttribute("id", "");
		ent.add(entity);
		ArrayList<Element> obj = getXML().search("entities", "entity");
		ArrayList<Element> rel = getXML().search("relations", "relation");
		
		ArrayList<String> ids_e = new ArrayList<String>();
		ArrayList<String> ids_o = new ArrayList<String>();
		ArrayList<String> ids_r = new ArrayList<String>();
		for( String key : listByCode(ENTITY).keySet() ) {
			if( !listByCode(ENTITY).get(key).isOFF() ) {
				ids_e.add(key);
			}
		}
		for( String key : listByCode(OBJECT).keySet() ) {
			if( !listByCode(OBJECT).get(key).isOFF() ) {
				ids_o.add(key);
			}
		}
		for( String key : listByCode(RELATION).keySet() ) {
			if( !listByCode(RELATION).get(key).isOFF() ) {
				ids_r.add(key);
			}
		}
		
		filterByIds( ent, ids_e, "id" );
		filterByIds( obj, ids_o, "id" );
		filterByIds( rel, ids_r, "id" );
		
		for( int i=rel.size()-1; i>=0; i-- ) {
			if( 
				!ids_o.contains(rel.get(i).getAttributeValue("from")) ||
				!ids_o.contains(rel.get(i).getAttributeValue("to"))
			) { rel.remove(i); }
		}
		
		// put it to the datafile
		Element root = new Element("onto");
		Document doc = new Document(root);
		doc.setRootElement(root);
		
		Element root_obj = null;
		root_obj = new Element("objects");
		for( Element e : obj ) {
			Element elem = (Element)e.clone();
			elem.setName("object");
			root_obj.addContent(elem);
		}
		doc.getRootElement().addContent(root_obj);
		
		root_obj = new Element("relations");
		for( Element e : rel ) {
			root_obj.addContent((Element)e.clone());
		}
		doc.getRootElement().addContent(root_obj);
		/*
		root_obj = new Element("entities");
		for( Element e : ent ) {
			root_obj.addContent((Element)e.clone());
		}
		doc.getRootElement().addContent(root_obj);	
		*/
		XMLOutputter xmlOutput = new XMLOutputter();
		xmlOutput.setFormat(Format.getPrettyFormat());
		try {
			xmlOutput.output(doc, new FileWriter(path));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		xmlf = new XMLManager(path);
	}
	
	public boolean getVertexStatus( TreeNode root ){
		return status(root.getId(), Dataset.OBJECT).isShown() &&
				status(root.getEntity(), Dataset.ENTITY).isShown();
	}
	public boolean getEdgeStatus(Relation e) {
		return listByCode(RELATION).containsKey(e.getId()) ? listByCode(RELATION).get(e.getId()).isShown() : true;
	}
	
	
	
	@Override
	public TreeNode getConnectedComponent(String focusNode, ArrayList<String> ids) {
		String focus = "";
		if( ids.contains(focusNode) ) {
			focus = focusNode;
		} else {
			ArrayList<Element> search = getXMLf().search("objects", "object", new String[]{"parent",""} );
			boolean found = false;
			for( Element e : search ) {
				if( ids.contains(e.getAttributeValue("id")) ) {
					focus = e.getAttributeValue("id");
				}
			}
			if( !found ) {
				focus = ids.get(0);
			}
		}
		
		TreeNode root = getTreeNodes().get(focus);
		root.setParent(null);
		
		ArrayList<String> treeNodeIds = new ArrayList<String>();
		ArrayList<String> wave = new ArrayList<String>();
		wave.add(focus);
		treeNodeIds.add(focus);
		
		ArrayList<Element> res = null;
		
		String[] fromto = new String[]{ "to", "from" } ;
		int k = 0;
		while( wave.size() !=0 ) {
			ArrayList<String> newWave = new ArrayList<String>();
			for( String id : wave ) {
				if( Configuration.get("isSubClass").equals("1") ) {
					// TODO How to behave when we don't list the relations
				} else {
					for(int i=Configuration.get("directEdges").equals("1")?1:0; i<=1; i++) {
						res = getXMLf().search(
							"relations", "relation", 
							new String[]{fromto[1-i],id}
						);
						for( Element e : res ) {
							String val = e.getAttributeValue(fromto[i]);
							if( treeNodeIds.contains(val) ) {
								getTreeNodes().get(id).addChildren(
									getTreeNodes().get(val), 
									new RelationPair(getRelations().get(e.getAttributeValue("id")), i!=0)
								);
							} else if( ids.contains(val) ) {
								newWave.add(val);
								treeNodeIds.add(val);
								getTreeNodes().get(id).addChildren(
									getTreeNodes().get(val), 
									new RelationPair(getRelations().get(e.getAttributeValue("id")), i!=0)
								);
							} 
							// TODO loops
						}
					}
				}
			}
			wave = newWave;
			//k++; if( k == 2 ) break;
		}
		
		for( String id : treeNodeIds ) {
			// TODO
			Visualizer.treeNodes.put(id, getTreeNodes().get(id));
			ids.remove(id);
		}
		return root;
	}
	
	
	/* INFO: vertex */
	public String getVertexLabel( TreeNode root ) {
		return getVertexName(root);
	};
	public String getVertexName( TreeNode root ) {
		if( root == null ) return "";
		if( vertexNames.containsKey(root) ) {
			return vertexNames.get(root);
		}
		String s = "";
		ArrayList<Element> res = getXML().search("entities", "entity", new String[]{"id",root.getId()});
		if( !res.isEmpty() ) {
			s = res.get(0).getAttributeValue("name");
		}
		vertexNames.put(root,s);
		return s;
	};
	public String getVertexFullName( TreeNode root ) {
		return getVertexName(root);
	};
	public String getVertexInfo( TreeNode root ) {
		if( vertexInfos.containsKey(root) ) {
			return vertexInfos.get(root);
		}
		String s = "<html><table>";
		ArrayList<Element> res = getXML().search("entities", "entity", new String[]{"id",root.getId()});
		if( !res.isEmpty() ) {
			Element elem = res.get(0);
			s += "<tr><td>"+"id"+"</td><td>" + elem.getAttributeValue("id") + "</td></tr>";
			s += "<tr><td>"+"Имя"+"</td><td>" + elem.getAttributeValue("name") + "</td></tr>";
			s += "<tr><td>"+"Родитель"+"</td><td>" + 
					getVertexName( getTreeNodes().get(elem.getAttributeValue("parent")))	
				+ "</td></tr>";
		}
		s += "</table></html>";
		vertexInfos.put(root,s);
		return s;
	};
	
	/* INFO: edge */
	public String getEdgeLabel( Relation rel ) {
		return getEdgeName(rel);
	};
	public String getEdgeName( Relation rel ) {
		if( rel == null ) return "";
		if( edgeNames.containsKey(rel) ) {
			return edgeNames.get(rel);
		}
		String s = "";
		ArrayList<Element> res = getXML().search("relations", "relation", new String[]{"id",rel.getId()});
		if( !res.isEmpty() ) {
			s = res.get(0).getAttributeValue("name");
		}
		edgeNames.put(rel,s);
		return s;
	};
	public String getEdgeInfo( Relation rel ) {
		if( edgeInfos.containsKey(rel) ) {
			return edgeInfos.get(rel);
		}
		String s = "<html><table>";
		ArrayList<Element> res = getXML().search("relations", "relation", new String[]{"id",rel.getId()});
		if( !res.isEmpty() ) {
			Element elem = res.get(0);
			s += "<tr><td>"+"id"+"</td><td>" + elem.getAttributeValue("id") + "</td></tr>";
			s += "<tr><td>"+"Первый агрумент:"+"</td><td>" + 
					getVertexName(getTreeNodes().get(elem.getAttributeValue("from"))) 
				+ "</td></tr>";
			s += "<tr><td>"+"Второй агрумент:"+"</td><td>" + 
					getVertexName(getTreeNodes().get(elem.getAttributeValue("to"))) 
				+ "</td></tr>";
		}
		s += "</table></html>";
		edgeInfos.put(rel,s);
		return s;
	};
	
	/* INFO: data */
	public String getEntityName( String id ) {
		return getVertexName(getTreeNodes().get(id));
	};
	public String getObjectName( String id ) {
		return getVertexName(getTreeNodes().get(id));
	};
	public String getRelationName( String id ) {
		return getEdgeName(getRelations().get(id));
	};
	
	
	
	/*
	public String getEntityName( String id ) {
		return getVertexShortLabel(getTreeNodes().get(id));
	}
	*/
}
