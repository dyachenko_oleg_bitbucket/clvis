package app.visualizer;


import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ChainedTransformer;

import app.data.Dataset;
import app.graph.Relation;
import app.graph.RelationPair;
import app.graph.TreeEdge;
import app.graph.TreeNode;
import app.visualizer.algorithm.Algorithm;
import app.visualizer.filters.EdgeFilter;
import app.visualizer.filters.VertexFilter;
import app.visualizer.info.EdgeLabeler;
import app.visualizer.info.EdgeStringer;
import app.visualizer.info.VertexLabeler;
import app.visualizer.info.VertexStringer;
import app.visualizer.layout.VisualizerLayoutTransition;
import app.visualizer.layout.VisualizerPixelTransformer;
import app.visualizer.layout.VisualizerTreeNodeTransformer;
import app.visualizer.mouse.EdgeStateListener;
import app.visualizer.mouse.VertexStateListener;
import app.visualizer.mouse.VisualizerMouseListener;
import app.visualizer.renderers.EdgeRenderer;
import app.visualizer.renderers.VertexRenderer;
import app.visualizer.renderers.VertexShaper;
import app.visualizer.zoom.Zoom;
import app.visualizer.zoom.ZoomMouse;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.StaticLayout;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.CrossoverScalingControl;
import edu.uci.ics.jung.visualization.control.ScalingControl;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import edu.uci.ics.jung.visualization.util.Animator;

public class Visualizer {
	public static ArrayList<Dataset> datasets = new ArrayList<Dataset>(); 
	private static int dsindex = 0;
	public static Dataset getDataset() {
		return datasets.get(dsindex);
	}
	public static void changeDataset() {
		dsindex = 1 - dsindex;
	}
	public static int getDSindex() {
		return dsindex;
	}
	
	public static ArrayList<TreeNode> currentList = new ArrayList<TreeNode>(); 
		
	public static DirectedSparseGraph<TreeNode, Relation> graph;
	public static Layout<TreeNode, Relation> layout;
	public static VisualizationViewer<TreeNode, Relation> vv;
	
	public static Map<String, TreeNode> treeNodes = new HashMap<String, TreeNode>();
	public static Map<TreeNode,Color> vertexColors = new HashMap<TreeNode, Color>();
	//public static VisualizerEdgeColorDelegate<TreeNode,Relation> edgeDrawPaint;
	public static EdgeRenderer edgeDrawPaint;
	
	// shapers
	public static VertexShaper vertexShaper = new VertexShaper();
	public static final Zoom scaler = new Zoom();
	public static final ZoomMouse graphMouse = new ZoomMouse();
	
	
	// stringers
	public static VertexStringer vertexStringer = new VertexStringer();
	public static EdgeStringer edgeStringer = new EdgeStringer();
	
	// filters
	public static VertexFilter vertexFilter = new VertexFilter(true);
	public static EdgeFilter edgeFilter = new EdgeFilter(false);
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void init() {
		graph = new DirectedSparseGraph<TreeNode, Relation>();
				
		layout = new StaticLayout<TreeNode, Relation>(graph,
	        new ChainedTransformer(new Transformer[]{
	        	new VisualizerTreeNodeTransformer(treeNodes),
	        	new VisualizerPixelTransformer()
	        }
	    ));
		vv =  new VisualizationViewer<TreeNode, Relation>(layout);
		
		/* EDGE SIZE */
		vv.getRenderContext().setVertexShapeTransformer(vertexShaper);
				
		/* FILTERS */
		vv.getRenderContext().setVertexIncludePredicate(vertexFilter);
		vv.getRenderContext().setEdgeIncludePredicate(edgeFilter);
		
		/* LISTENERS */
		vv.setGraphMouse(graphMouse);
        vv.addKeyListener(graphMouse.getModeKeyListener());
        
        /* INFO */
        vv.getPickedVertexState().addItemListener(new VertexStateListener());
        vv.getPickedEdgeState().addItemListener(new EdgeStateListener());
        vv.addGraphMouseListener(new VisualizerMouseListener());
        
        /* LABELERS */
        vv.setVertexToolTipTransformer(new VertexLabeler());
        vv.setEdgeToolTipTransformer(new EdgeLabeler());
        vv.getRenderContext().setVertexLabelTransformer(vertexStringer);
        vv.getRenderer().getVertexLabelRenderer().setPosition(Renderer.VertexLabel.Position.AUTO);
        vv.getRenderContext().setEdgeLabelTransformer(edgeStringer);
        
        /* RENDERERS */
        vv.getRenderer().setVertexRenderer( new VertexRenderer(vv.getPickedVertexState(), false) );
        edgeDrawPaint = new EdgeRenderer(vv);
		vv.getRenderContext().setEdgeDrawPaintTransformer( edgeDrawPaint );
		vv.getRenderContext().setArrowFillPaintTransformer(edgeDrawPaint.arrowFillPaintTransformer);
        vv.getRenderContext().setArrowDrawPaintTransformer(edgeDrawPaint.arrowDrawPaintTransformer);
        vv.getRenderContext().getEdgeLabelRenderer().setRotateEdgeLabels(false);
        vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line<TreeNode,Relation>());
        
    }
	
	public static void removeEdges() {
		ArrayList<Relation> list = new ArrayList<Relation>( Visualizer.graph.getEdges() );
		for( Relation edge : list ) {
			Visualizer.graph.removeEdge(edge);
		}
		Visualizer.vv.repaint();
		Visualizer.treeNodes.clear();
	}
	public static void removeVertices() {
		ArrayList<TreeNode> list = new ArrayList<TreeNode>( Visualizer.graph.getVertices() );
		for( TreeNode tn : list ) {
			Visualizer.graph.removeVertex(tn);
		}
		Visualizer.vv.repaint();
	}
	
	public static void createVertices( TreeNode root ) {
		Visualizer.graph.addVertex( root );
		for( TreeNode treeNode : root.getChildren() ) {
			createVertices(treeNode);
		}
	}
	public static void createVertices( ArrayList<TreeNode> list ) {
		for( TreeNode root : list ) {
			createVertices(root);
		}
	}
	
	public static void createEdges( TreeNode root ) {
		for( TreeEdge te : root.getEdges() ) {
			RelationPair rp = te.getRelationPair();
			if( !Visualizer.graph.getEdges().contains(rp.getRelation())) {
				Visualizer.graph.addEdge(
					rp.getRelation(),
					rp.getDirection() ? root : te.getTreeNode(),
					!rp.getDirection() ? root : te.getTreeNode(),
					EdgeType.DIRECTED);
			}
		}
		for( TreeNode treeNode : root.getChildren() ) {
			createEdges(treeNode);
		}
	}
	public static void createEdges( ArrayList<TreeNode> list ) {
		for( TreeNode root : list ) {
			createEdges(root);
		}
	}
	
	public static void changeVisualization( boolean changeDataset, int alg_code ) {
		Algorithm alg = Algorithm.getInstance(Algorithm.code = alg_code);
		if( changeDataset ) {
			removeEdges();
			removeVertices();
			changeDataset();
		}
		String focusNode = getDataset().getFocus();
		ArrayList<TreeNode> list = getDataset().getConnectedComponents(focusNode);
		Visualizer.currentList = list;
		alg.setCoordinates(list);
				
		if( graph.getVertexCount() == 0 ) {
			createVertices(list);
	        createEdges(list);
		}
		
		if( !changeDataset ) {
			Visualizer.vv.getRenderContext().getMultiLayerTransformer().setToIdentity();
			Layout<TreeNode, Relation> layout = new StaticLayout<TreeNode, Relation>(Visualizer.graph,
			        new ChainedTransformer(new Transformer[]{
			        	new VisualizerTreeNodeTransformer(Visualizer.treeNodes),
			        	new VisualizerPixelTransformer()
			        }
			    ));
			VisualizerLayoutTransition<TreeNode, Relation> vlt = 
				new VisualizerLayoutTransition<TreeNode, Relation>(
					Visualizer.vv,
					Visualizer.layout,
					layout
				);
			Animator animator = new Animator(vlt);
			animator.start();
		}
		
		ScalingControl scaler = new CrossoverScalingControl();
		scaler.scale(vv, 1f, vv.getCenter());
		Visualizer.vv.repaint();
	}
	
	private static void updateLayout() {
		for( String id: treeNodes.keySet() ) {
			layout.setLocation(
				treeNodes.get(id), 
				new Point2D.Double(
					Math.round(treeNodes.get(id).pos.x),
					Math.round(treeNodes.get(id).pos.y)
				));
		}
		vv.repaint();
	}
	
	public static void repaintVisualization() {
		getDataset().clean();
		getDataset().applyFilters(getDataset().path);
		Visualizer.changeDataset();
		changeVisualization(true, Algorithm.code);
		Visualizer.vv.repaint();
		updateLayout();
	}
	
	public static void setCenter( TreeNode root ) {
		Point2D center = new Point2D.Double(Visualizer.vv.getWidth()/2, Visualizer.vv.getHeight()/2);
		Point2D root_pt = Visualizer.layout.transform(root);
		Point2D move = new Point2D.Double( 
			center.getX() - root_pt.getX(),
			center.getY() - root_pt.getY()
		);
		
		Layout<TreeNode, Relation> layout = new StaticLayout<TreeNode, Relation>(Visualizer.graph,
		        new ChainedTransformer(new Transformer[]{
		        	new VisualizerTreeNodeTransformer(Visualizer.treeNodes),
		        	new VisualizerPixelTransformer()
		        }
		    ));
		for( TreeNode tn : Visualizer.layout.getGraph().getVertices() ) {
			Point2D pt = Visualizer.layout.transform(tn);
			layout.setLocation(tn, new Point2D.Double(pt.getX()+move.getX(),pt.getY()+move.getY()));
		}
		VisualizerLayoutTransition<TreeNode, Relation> vlt = 
			new VisualizerLayoutTransition<TreeNode, Relation>(
				Visualizer.vv,
				Visualizer.layout,
				layout
			);
		Animator animator = new Animator(vlt);
		animator.start();
	}
	
	
	
	
	
	
}
