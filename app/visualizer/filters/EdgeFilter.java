package app.visualizer.filters;

import org.apache.commons.collections15.Predicate;

import app.graph.Relation;
import app.graph.TreeNode;
import app.visualizer.Visualizer;
import app.visualizer.colorers.AnalysisColorer;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.Context;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.graph.util.Pair;

public class EdgeFilter implements Predicate<Context<Graph<TreeNode,Relation>,Relation>> {
	
	boolean showBackReferences = false; 
	
	public EdgeFilter( boolean showBackReferences ) {
		this.showBackReferences = showBackReferences;
	}

	public void showBackReferences( boolean flag ) {
		this.showBackReferences = flag;
	}

	public boolean evaluate(Context<Graph<TreeNode,Relation>,Relation> context) {
		Graph<TreeNode,Relation> graph = context.graph;
		Relation e = context.element;
	    Pair<TreeNode> pair = graph.getEndpoints(e);
		
	    if( AnalysisColorer.state() ) {
			return true;
		}
	    
	    if( !Visualizer.getDataset().getEdgeStatus(e) ) {
	    	return false;
	    }
	    if( showBackReferences ) return true;
	    if( 
	    	Visualizer.vv.getPickedVertexState().isPicked(pair.getFirst()) ||
	    	Visualizer.vv.getPickedVertexState().isPicked(pair.getSecond())
	    ) {
	    	return true;
	    }
	    if( 
	    	!pair.getSecond().hasParent(pair.getFirst()) &&
	    	!pair.getFirst().hasParent(pair.getSecond())
	    ) {
	    	return false;
	    }
	    return true;
	}

}
