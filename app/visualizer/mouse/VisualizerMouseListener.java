package app.visualizer.mouse;

import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;

import app.graph.TreeNode;
import app.visualizer.Visualizer;
import app.visualizer.algorithm.Algorithm;
import edu.uci.ics.jung.visualization.control.GraphMouseListener;

public class VisualizerMouseListener implements GraphMouseListener<TreeNode>  {
	
	@Override
	public void graphClicked(TreeNode tn, MouseEvent e) {
		if( e.isControlDown() ) {
			Visualizer.setCenter( tn );
		} else if( e.isShiftDown() ) {
			tn.clean();
			Visualizer.getDataset().setFocus( tn.getId() );
			Visualizer.changeVisualization( false, Algorithm.code );
		} else if( e.getClickCount() > 1 ) {
			Visualizer.vertexStringer.put(tn, false);
			JOptionPane.showMessageDialog(null, Visualizer.getDataset().getVertexInfo(tn));
		}		
	}

	@Override
	public void graphPressed(TreeNode arg0, MouseEvent arg1) {
		//Visualizer.graph.removeEdge(1);
		//System.out.println("removed");
	}

	@Override
	public void graphReleased(TreeNode arg0, MouseEvent arg1) {
		//
	}

}
