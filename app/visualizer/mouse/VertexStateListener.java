package app.visualizer.mouse;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import app.graph.TreeNode;
import app.visualizer.Visualizer;

import edu.uci.ics.jung.visualization.picking.MultiPickedState;

public class VertexStateListener implements ItemListener {

	@Override
	public void itemStateChanged(ItemEvent arg0) {
		@SuppressWarnings("rawtypes")
		Object[] clicked = ((MultiPickedState)arg0.getSource()).getSelectedObjects();
		for( Object obj : clicked ) {
			Visualizer.vertexStringer.put((TreeNode)obj);
		}
	}

}
