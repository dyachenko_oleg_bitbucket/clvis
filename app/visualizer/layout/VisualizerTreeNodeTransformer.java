package app.visualizer.layout;


import java.util.Map;

import org.apache.commons.collections15.Transformer;

import app.graph.TreeNode;

public class VisualizerTreeNodeTransformer implements Transformer<TreeNode,TreeNode> {
	Map<String,TreeNode> map;
    public VisualizerTreeNodeTransformer(Map<String,TreeNode> map) {
    	this.map = map;
    }
    public TreeNode transform(TreeNode tn) {
		return transform(tn.getId());
	}
	public TreeNode transform(String key) {
		return map.get(key);
	}
	public void update(TreeNode tn) {
		map.put(tn.getId(), tn);
	}
}
