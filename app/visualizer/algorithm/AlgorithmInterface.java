package app.visualizer.algorithm;


import java.util.ArrayList;

import app.graph.Point;
import app.graph.TreeNode;

public interface AlgorithmInterface {
	
	public void setCoordinates( TreeNode root );
	public void setCoordinates( ArrayList<TreeNode> list );
	
	public Point max( TreeNode root );
	public Point min( TreeNode root );
	public Point size( TreeNode root );
	
	
}
