package app.visualizer.algorithm;


import java.util.ArrayList;
import java.util.HashMap;

import app.graph.Point;
import app.graph.TreeNode;

public class LevelLayout extends Algorithm {
	
	// abs_x rel_x 
	HashMap<TreeNode, Point> rData = new HashMap<TreeNode, Point>();
	HashMap<TreeNode, Point> lData = new HashMap<TreeNode, Point>();
	//HashMap<TreeNode, Point> data = new HashMap<TreeNode, Point>();
	private HashMap<TreeNode, Point> getData( boolean left ) {
		return left ? lData : rData;
	}
	
	
	HashMap<TreeNode, ArrayList<TreeNode>> rContours = new HashMap<TreeNode, ArrayList<TreeNode>>();
	HashMap<TreeNode, ArrayList<TreeNode>> lContours = new HashMap<TreeNode, ArrayList<TreeNode>>();
		
	@Override
	public void setCoordinates(TreeNode root) {
		root.setLevels();
		initDataset(root);
		initContours(root);
		setLCoordinatesStep(root);
		setRCoordinatesStep(root);
		setAbsPositions(root);
	}
	@Override
	public void setCoordinates(ArrayList<TreeNode> list) {
		for( TreeNode root : list ) {
			setCoordinates(root);
		}
		// TODO Think of cool layout for CCs
		placeComponents(list);
		/*
		double ct = 0;
		for( int i=1; i<list.size(); i++ ) {
			move(list.get(i), new Point( max(list.get(i-1)).x + list.get(i).pos.x - min(list.get(i)).x + 2 , 0));
		}
		*/
	}	
	
	/* to get symmetric picture we first get L-side level position and then R-side, average x-coordinate is resulting one */
	
	private void setLCoordinatesStep( TreeNode root ) {
		lData.get(root).sety(0).setx(0);
		ArrayList<TreeNode> childs = root.getChildren();
		if( childs.isEmpty() ) {
			// Nothing 
		} else {
			for( TreeNode tn : childs ) {
				setLCoordinatesStep(tn);
			}
			ArrayList<TreeNode> rt = rContours.get( childs.get(0) );
			ArrayList<Double> rtp = contourPosition(rt, true);
			for( int i=1; i<childs.size(); i++ ) {
				ArrayList<TreeNode> lt = lContours.get( childs.get(i) );
				ArrayList<Double> ltp = contourPosition(lt, true);
				double max = 0;
				for( int j=0; j<ltp.size() && j<rtp.size(); j++ ) {
					double val = 
						rtp.get(j) -
						ltp.get(j) + 1;
					if( max < val ) {
						max = val;
					}
				}
				getData(true).get( childs.get(i) ).sety(
					getData(true).get( childs.get(i-1) ).y + max
				);
				// recalculate the right contour
				ArrayList<Double> rt2 = contourPosition(rContours.get(childs.get(i)), true);
				ArrayList<Double> rtnew = new ArrayList<Double>();
				for( int j=0; j<rt2.size(); j++ ) {
					rtnew.add(rt2.get(j));
				}
				for( int j=rt2.size(); j<rtp.size(); j++ ) {
					rtnew.add(rtp.get(j) - max);
				}
				rtp = rtnew;
			}
			double max = getData(true).get( childs.get(childs.size()-1) ).y; 
			for( TreeNode tn : childs ) {
				getData(true).get( tn ).sety( getData(true).get(tn).y - max * 0.5 );
				recalculateAbs(tn, true);
			}
		}
	}
	private void setRCoordinatesStep( TreeNode root ) {
		rData.get(root).sety(0).setx(0);
		ArrayList<TreeNode> childs = root.getChildren();
		if( childs.isEmpty() ) {
			// Nothing 
		} else {
			for( TreeNode tn : childs ) {
				setRCoordinatesStep(tn);
			}
			ArrayList<TreeNode> rt = lContours.get( childs.get(childs.size()-1) );
			ArrayList<Double> rtp = contourPosition(rt, false);
			for( int i=childs.size()-2; i>=0; i-- ) {
				ArrayList<TreeNode> lt = rContours.get( childs.get(i) );
				ArrayList<Double> ltp = contourPosition(lt, false);
				double max = 0;
				for( int j=0; j<ltp.size() && j<rtp.size(); j++ ) {
					double val = 
						ltp.get(j) - 
						rtp.get(j) + 1;
					if( max < val ) {
						max = val;
					}
				}
				getData(false).get(childs.get(i)).sety(
					getData(false).get(childs.get(i+1)).y - max
				);
				// recalculate the left contour
				ArrayList<Double> rt2 = contourPosition(lContours.get(childs.get(i)), false);
				ArrayList<Double> rtnew = new ArrayList<Double>();
				for( int j=0; j<rt2.size(); j++ ) {
					rtnew.add(rt2.get(j));
				}
				for( int j=rt2.size(); j<rtp.size(); j++ ) {
					rtnew.add(rtp.get(j) + max);
				}
				rtp = rtnew;
			}
			double max = getData(false).get( childs.get(0) ).y;
			for( TreeNode tn : childs ) {
				getData(false).get( tn ).sety( getData(false).get(tn).y - 0.5*max );
				recalculateAbs(tn, false);
			}
		}
	}

	
	
	// init
	private void initDataset(TreeNode root) {
		lData.put(root, new Point(0,0));
		rData.put(root, new Point(0,0));
		for( TreeNode tn : root.getChildren() ) {
			initDataset(tn);
		}
	}
	private void initContours(TreeNode root) {
		ArrayList<TreeNode> cont = new ArrayList<TreeNode>();
		ArrayList<TreeNode> childs = root.getChildren();
		if( childs.isEmpty() ) {
			// left
			cont = new ArrayList<TreeNode>();
			cont.add(root);
			lContours.put(root, cont);
			// right
			cont = new ArrayList<TreeNode>();
			cont.add(root);
			rContours.put(root, cont);
		} else {
			for( TreeNode tn : childs ) {
				initContours(tn);
			}
			// left
			cont = new ArrayList<TreeNode>();
			cont.add(root);
			for( int i=0; i<childs.size(); i++ ) {
				ArrayList<TreeNode> tmp = lContours.get(childs.get(i));
				if( tmp.size() + 1 > cont.size() ) {
					for( int j=cont.size(); j<tmp.size()+1; j++ ) {
						cont.add(tmp.get(j-1));
					}
				}
			}
			lContours.put(root, cont);
			// right
			cont = new ArrayList<TreeNode>();
			cont.add(root);
			for( int i=childs.size()-1; i>=0; i-- ) {
				ArrayList<TreeNode> tmp = rContours.get(childs.get(i));
				if( tmp.size() + 1 > cont.size() ) {
					for( int j=cont.size(); j<tmp.size()+1; j++ ) {
						cont.add(tmp.get(j-1));
					}
				}
			}
			rContours.put(root, cont);
		}
	}
	private ArrayList<Double> contourPosition( ArrayList<TreeNode> contour, boolean left ) {
		ArrayList<Double> out = new ArrayList<Double>();
		for( TreeNode tn : contour ) {
			out.add( getData(left).get(tn).x );
		}
		return out;
	}
	private void setAbsPositions( TreeNode root ) {
		root.pos = new Point( 
			( lData.get(root).x + rData.get(root).x ) * 0.5,
			// 0.25
			root.getLevel()
		);
		ArrayList<TreeNode> childs = root.getChildren();
		for( TreeNode tn : childs ) {
			setAbsPositions(tn);
		}
	}
	private void recalculateAbs( TreeNode root, boolean left ) {
		recalculateAbs(root, getData(left).get(root).y, left);
	}
	private void recalculateAbs( TreeNode root, double shift, boolean left ) {
		getData(left).get(root).movex(shift);
		for( TreeNode tn : root.getChildren() ) {
			recalculateAbs(tn, shift, left);
		}
	}
	
	
	
}
