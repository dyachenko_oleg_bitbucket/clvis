package app.visualizer.algorithm;


import java.util.ArrayList;
import java.util.HashMap;

import app.graph.Point;
import app.graph.TreeEdge;
import app.graph.TreeNode;


public class FruchtermanReingoldLayout extends Algorithm {
	
	private int stepCount = 100; 
	public double delta( int step ) {
		return 0.1;
	}
	double l = 1;
			
	
	// force 
	HashMap<TreeNode, ArrayList<TreeNode>> nodes = new HashMap<TreeNode, ArrayList<TreeNode>>();
	//HashMap<TreeNode, ArrayList<Pair<TreeNode>>> edges = new HashMap<TreeNode, ArrayList<Pair<TreeNode>>>();
	
	private double fr( double x, double l ) {
		return l*l/x;
	}
	private double fa( double x, double l ) {
		return x*x/l;
	}
	private double temp(int step) {
		return 5/Math.log(step+1);
	}
	
	
	@Override
	public void setCoordinates(TreeNode root) {
		root.setLevels();
		if( !nodes.containsKey(root) ) {
			nodes.put(root,root.getAllNodes());
		}
		for( int i=1; i<=stepCount; i++ ) {
			setCoordinatesStep(root, i);
		}
	}
	@Override
	public void setCoordinates(ArrayList<TreeNode> list) {
		for( TreeNode root : list ) {
			setCoordinates(root);
		}
		// TODO Think of cool layout for CCs
		placeComponents(list);
	}	
	
	private void setCoordinatesStep( TreeNode root, int step ) {
		ArrayList<TreeNode> _nodes = nodes.get(root);
		//ArrayList<Pair<TreeNode>> _edges = edges.get(root);
		HashMap<TreeNode, Point> forces = new HashMap<TreeNode, Point>();
		
		for( TreeNode v: _nodes ) {
			forces.put(v, new Point(0,0));
			for( TreeNode u: _nodes ) {
				if( !u.equals(v) ) {
					Point force = v.pos.clone().move(u.pos.clone().minus());
					double dist = force.abs(); 
					force.mult(1/dist);
					forces.get(v).move( force.clone().mult(fr(dist,l)) );
				}
			}
			for( TreeEdge te : v.getEdges() ) {
				Point force = v.pos.clone().move(te.getTreeNode().pos.clone().minus());
				double dist = force.abs(); 
				force.mult(1/dist);
				forces.get(v).move( force.clone().mult(-fa(dist,l)) );
			}
		}
		
		for( TreeNode v: _nodes ) {
			Point pt = forces.get(v).clone();
			double pt_dist = pt.abs();
			if( pt_dist > temp(step) ) {
				pt.mult(temp(step));
				pt.mult((double)1/pt_dist);
			}
			v.pos.move(pt);
			
			/*
			if( v.pos.x < -(double)App.WIDTH/VisualizerPixelTransformer.resize ) {
				v.pos.x = -(double)App.WIDTH/VisualizerPixelTransformer.resize;
			}
			if( v.pos.y < -(double)App.HEIGHT/VisualizerPixelTransformer.resize ) {
				v.pos.y = -(double)App.HEIGHT/VisualizerPixelTransformer.resize;
			}
			if( v.pos.x > (double)App.WIDTH/VisualizerPixelTransformer.resize ) {
				v.pos.x = (double)App.WIDTH/VisualizerPixelTransformer.resize;
			}
			if( v.pos.y > (double)App.HEIGHT/VisualizerPixelTransformer.resize ) {
				v.pos.y = (double)App.HEIGHT/VisualizerPixelTransformer.resize;
			}
			*/
		}
	}
}
