package app.visualizer.info;


import org.apache.commons.collections15.Transformer;

import app.graph.Relation;
import app.visualizer.Visualizer;


public class EdgeLabeler implements Transformer<Relation,String> {

	@Override
	public String transform(Relation rel) {
		return Visualizer.getDataset().getEdgeLabel(rel);
	}
}