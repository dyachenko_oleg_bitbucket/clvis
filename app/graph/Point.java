package app.graph;

public class Point {
	public double x = 0;
	public double y = 0;
	
	public Point movex( double x ) {
		this.x += x;
		return this;
	}
	public Point movey( double y ) {
		this.y += y;
		return this;
	}
	public Point move( Point pt ) {
		return this.movex(pt.x).movey(pt.y);
	}
		
	public Point setx( double x ) {
		this.x = x;
		return this;
	}
	public Point sety( double y ) {
		this.y = y;
		return this;
	}
	public Point set( Point pt ) {
		return this.setx(pt.x).sety(pt.y);
	}
	public Point mult( double multiplier ) {
		return this.setx(multiplier*x).sety(multiplier*y);
	}
	public Point minus() {
		return this.setx(-x).sety(-y);
	}
	public double abs() {
		return Math.sqrt( x*x + y*y );
	}
	
	
	public String toString() {
		return "[" + x + "; " + y + "]";
	}
	
	public static Point max(Point pt1, Point pt2) {
		return new Point( pt1.x>=pt2.x ? pt1.x : pt2.x, pt1.y>=pt2.y ? pt1.y : pt2.y );
	}
	public static Point min(Point pt1, Point pt2) {
		return new Point( pt1.x<=pt2.x ? pt1.x : pt2.x, pt1.y<=pt2.y ? pt1.y : pt2.y );
	}
	
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	public Point(Point pt) {
		this.x = pt.x;
		this.y = pt.y;
	}
	public Point clone() {
		return new Point(this);
	}
	
}
