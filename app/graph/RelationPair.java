package app.graph;

public class RelationPair {
	private Relation relation = null;
	private boolean direction = true;
	/*
	 * @code true  : direct
	 * @code false : reverse
	 */
	
	public Relation getRelation() {
		return relation;
	}
	public Relation setRelation(Relation relation) {
		this.relation = relation;
		return this.relation;
	}
	
	public boolean getDirection() {
		return direction;
	}
	public boolean setDirection(boolean direction) {
		this.direction = direction;
		return this.direction;
	}
	
	public RelationPair(Relation relation, boolean direction) {
		setRelation(relation);
		setDirection(direction);
	}
	 
}
