package app.graph;

import java.util.ArrayList;

public class ArrayListClone<T> {
	public ArrayList<T> get( ArrayList<T> list ) {
		ArrayList<T> clone = new ArrayList<T>();
		for( T e : list ) {
			clone.add(e);
		}
		return clone;
	}
}
