package app.graph;


public class TreeEdge {

	private RelationPair relationPair = null;
	private TreeNode treeNode = null;
			
	public RelationPair getRelationPair() {
		return relationPair;
	}
	public RelationPair setRelationPair(RelationPair relationPair) {
		this.relationPair = relationPair;
		return this.relationPair;
	}
	public TreeNode getTreeNode() {
		return treeNode;
	}
	public TreeNode setTreeNode(TreeNode treeNode) {
		this.treeNode = treeNode;
		return this.treeNode;
	}
	public boolean getDirection() {
		return relationPair.getDirection();
	}
	public boolean setDirection( boolean direction ) {
		return relationPair.setDirection(direction);
	}
	
	// constructor
	public TreeEdge(TreeNode treeNode, Relation relation, boolean direction) {
		setRelationPair(new RelationPair(relation, direction));
		this.treeNode = treeNode;
	}
	public TreeEdge(TreeNode treeNode, RelationPair relationPair) {
		setRelationPair(relationPair);
		this.treeNode = treeNode;
	}
}
