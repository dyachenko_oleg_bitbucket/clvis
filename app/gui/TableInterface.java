package app.gui;



public interface TableInterface extends AbstractModelInterface {
	public void addRenderer(int col);
	public void setColWidth(int col, int width);
	public AbstractModel getTableModel();
}
