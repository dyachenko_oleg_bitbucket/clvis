package app.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import app.config.Configuration;
import app.data.Dataset;
import app.graph.Relation;
import app.graph.TreeNode;
import app.visualizer.Visualizer;
import app.visualizer.algorithm.Algorithm;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse.Mode;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;

public class MenuBar extends JMenuBar{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5515079934174057500L;
	
	public final JMenu menu1 = new JMenu("Настройки");
	public final JMenu menu11 = new JMenu("Режим мыши");
	public final JCheckBoxMenuItem menu111 = new JCheckBoxMenuItem("Трансформация");
	public final JCheckBoxMenuItem menu112 = new JCheckBoxMenuItem("Выбор и перемещение");
	public final JMenu menu12 = new JMenu("Тип ребер");
	public final JCheckBoxMenuItem menu121 = new JCheckBoxMenuItem("Прямые");
	public final JCheckBoxMenuItem menu122 = new JCheckBoxMenuItem("Клинообразные");
	public final JCheckBoxMenuItem menu123 = new JCheckBoxMenuItem("Ортогональные");
	public final JCheckBoxMenuItem menu124 = new JCheckBoxMenuItem("Квадратичные");
	public final JCheckBoxMenuItem menu125 = new JCheckBoxMenuItem("Кубические");
	public final JCheckBoxMenuItem menu126 = new JCheckBoxMenuItem("Ломаные");
	public final JMenuItem menu13 = new JMenuItem("Раскраска вершин");
	//public final JCheckBoxMenuItem menu131 = new JCheckBoxMenuItem("Одноцветная");
	//public final JCheckBoxMenuItem menu132 = new JCheckBoxMenuItem("Разноцветная");
	public final JMenu menu14 = new JMenu("Прорисовка ребер");
	public final JCheckBoxMenuItem menu141 = new JCheckBoxMenuItem("Заполнение");
	public final JCheckBoxMenuItem menu142 = new JCheckBoxMenuItem("Градиент");
	public final JMenuItem menu15 = new JMenuItem("Раскраска ребер");
	//public final JMenuItem menu151 = new JMenuItem("Градиент 1");
	//public final JMenuItem menu152 = new JMenuItem("Цвет ребра / Градиент 2");
	//public final JMenuItem menu153 = new JMenuItem("Цвет выделенного ребра");
	public final JMenu menu16 = new JMenu("Лейблы");
	public final JCheckBoxMenuItem menu161 = new JCheckBoxMenuItem("Только название");
	public final JCheckBoxMenuItem menu162 = new JCheckBoxMenuItem("Полное описание");
	public final JMenuItem menu_save = new JMenuItem("Сохранить");
	public final JMenuItem menu_exit = new JMenuItem("Выйти");
	
	public final JMenu menu2 = new JMenu("Алгоритмы");
	public final JCheckBoxMenuItem menu21 = new JCheckBoxMenuItem("Поуровневый (Reinghold-Tilford)");
	public final JCheckBoxMenuItem menu22 = new JCheckBoxMenuItem("Радиальный");
	public final JCheckBoxMenuItem menu23 = new JCheckBoxMenuItem("Круговой (с размещ. по внешней окр-сти)");
	public final JCheckBoxMenuItem menu24 = new JCheckBoxMenuItem("Фрактальный");
	public final JCheckBoxMenuItem menu25 = new JCheckBoxMenuItem("Силовой (Spring embedder)");
	public final JCheckBoxMenuItem menu26 = new JCheckBoxMenuItem("Силовой (Fruchterman & Reingold)");
	public final JCheckBoxMenuItem menu27 = new JCheckBoxMenuItem("Силовой (Frick)");
	public final JCheckBoxMenuItem menu28 = new JCheckBoxMenuItem("Силовой (Kamada & Kawai)");
	
	public final JMenu menu3 = new JMenu("Фильтры");
	public final JMenu menu31 = new JMenu("Вершины");
	public final JMenuItem menu311 = new JMenuItem("По id объекта");
	public final JMenuItem menu312 = new JMenuItem("По id сущности");
	public final JMenuItem menu32 = new JMenuItem("Ребра");
	public final JMenuItem menu33 = new JMenuItem("Удалить фильтрованные");
	public final JMenu menu34 = new JMenu("Показывать ребра");
	public final JCheckBoxMenuItem menu341 = new JCheckBoxMenuItem("Все");
	public final JCheckBoxMenuItem menu342 = new JCheckBoxMenuItem("Остовного дерева");
	public final JMenu menu35 = new JMenu("Связность компонент");
	public final JCheckBoxMenuItem menu351 = new JCheckBoxMenuItem("Слабая");
	public final JCheckBoxMenuItem menu352 = new JCheckBoxMenuItem("Сильная");
	
	public final JMenu menu4 = new JMenu("Данные");
	public final JCheckBoxMenuItem menu41 = new JCheckBoxMenuItem("Объекты");
	public final JCheckBoxMenuItem menu42 = new JCheckBoxMenuItem("Онтология");
	
	public MenuBar() {
		super();
		ButtonGroup menu11bg = new ButtonGroup();
		menu11bg.add(menu111);
		menu11bg.add(menu112);
		menu111.setSelected(true);
		menu111.setToolTipText(
			"<html>"+
			"Перетаскивание мышью для управления изображением<br>"+
			"Скроллинг для увеличения и уменьшения"+
			"</html>"
		);
		menu111.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.graphMouse.setMode(Mode.TRANSFORMING);
			}
		});
		menu112.setToolTipText(
			"<html>"+
			"Левый клик - выделение вершины<br>"+
			"Правый клик - информация о вершине<br>"+
			"Ctrl + левый клик - поместить вершину в центр<br>"+
			"Ctrl + правый клик - сделать вершину фокусной"+
			"</html>"
		);
		menu112.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.graphMouse.setMode(Mode.PICKING);
			}
		});
		menu11.add(menu111);
		menu11.add(menu112);
		
		ButtonGroup menu12bg = new ButtonGroup();
		menu12bg.add(menu121);
		menu12bg.add(menu122);
		menu12bg.add(menu123);
		menu12bg.add(menu124);
		menu12bg.add(menu125);
		menu12bg.add(menu126);
		menu121.setSelected(true);
		menu121.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line<TreeNode,Relation>());
				Visualizer.vv.repaint();				
			}
		});
		menu122.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Wedge<TreeNode,Relation>(5));
				Visualizer.vv.repaint();				
			}
		});
		menu123.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Orthogonal<TreeNode,Relation>());
				Visualizer.vv.repaint();				
			}
		});
		menu124.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.QuadCurve<TreeNode,Relation>());
				Visualizer.vv.repaint();				
			}
		});
		menu125.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.CubicCurve<TreeNode,Relation>());
				Visualizer.vv.repaint();				
			}
		});
		menu126.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.BentLine<TreeNode,Relation>());
				Visualizer.vv.repaint();				
			}
		});
		menu12.add(menu121);
		menu12.add(menu122);
		menu12.add(menu123);
		menu12.add(menu124);
		menu12.add(menu125);
		menu12.add(menu126);
		
		menu13.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					Visualizer.getDataset().getVertexColorer().getColorChooser();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		ButtonGroup menu14bg = new ButtonGroup();
		menu14bg.add(menu141);
		menu14bg.add(menu142);
		menu142.setSelected(true);
		menu141.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.edgeDrawPaint.setGradient(false);
				Visualizer.vv.repaint();	
			}
		});
		menu142.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.edgeDrawPaint.setGradient(true);
				Visualizer.vv.repaint();	
			}
		});
		menu14.add(menu141);
		menu14.add(menu142);
		
		
		menu15.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					Visualizer.getDataset().getEdgeColorer().getColorChooser();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		ButtonGroup menu16bg = new ButtonGroup();
		menu16bg.add(menu161);
		menu16bg.add(menu162);
		menu161.setSelected(true);
		menu161.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		menu162.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					
			}
		});
		menu16.add(menu161);
		menu16.add(menu162);
				
		menu1.add(menu11);
		menu1.add(menu12);
		menu1.add(menu13);
		menu1.add(menu14);
		menu1.add(menu15);
		menu1.add(menu16);
		menu1.addSeparator();
		menu1.add(menu_save);
		menu1.addSeparator();
		menu1.add(menu_exit);
		
		ButtonGroup menu2bg = new ButtonGroup();
		menu2bg.add(menu21);
		menu2bg.add(menu22);
		menu2bg.add(menu23);
		menu2bg.add(menu24);
		menu2bg.add(menu25);
		menu2bg.add(menu26);
		menu2bg.add(menu27);
		menu2bg.add(menu28);
		menu22.setSelected(true);
		menu21.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.changeVisualization( false, Algorithm.ALG_LEVEL );
			}
		});
		menu22.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.changeVisualization( false, Algorithm.ALG_RADIAL );
			}
		});
		menu23.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.changeVisualization( false, Algorithm.ALG_CIRCLE );
			}
		});
		menu24.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.changeVisualization( false, Algorithm.ALG_BALLOON );
			}
		});
		menu25.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.changeVisualization( false, Algorithm.ALG_SPRING );
			}
		});
		menu26.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.changeVisualization( false, Algorithm.ALG_FRRE );
			}
		});
		menu27.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.changeVisualization( false, Algorithm.ALG_FRICK );
			}
		});
		menu28.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.changeVisualization( false, Algorithm.ALG_KK );
			}
		});
		
		menu2.add(menu21);
		menu2.add(menu22);
		menu2.add(menu23);
		menu2.add(menu24);
		menu2.add(menu25);
		menu2.add(menu26);
		menu2.add(menu27);
		menu2.add(menu28);
		
		ButtonGroup menu34bg = new ButtonGroup();
		menu34bg.add(menu341);
		menu34bg.add(menu342);
		menu342.setSelected(true);
		
		ButtonGroup menu35bg = new ButtonGroup();
		menu35bg.add(menu351);
		menu35bg.add(menu352);
		if( Configuration.get("directEdges").equals("1") ) {
			menu352.setSelected(true);
		} else {
			menu351.setSelected(true);
		}
		
		menu342.setSelected(true);
		menu311.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Visualizer.getDataset().openFilterDialog(Dataset.OBJECT);
				} catch (Exception e) {}
			}
		});
		menu312.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Visualizer.getDataset().openFilterDialog(Dataset.ENTITY);
				} catch (Exception e) {}
			}
		});
		menu32.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Visualizer.getDataset().openFilterDialog(Dataset.RELATION);
				} catch (Exception e) {}
			}
		});
		menu33.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Visualizer.getDataset().off("40", Dataset.OBJECT);
				Visualizer.repaintVisualization();
			}
		});
		menu341.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.edgeFilter.showBackReferences(true);
				Visualizer.vv.repaint();
			}
		});
		menu342.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Visualizer.edgeFilter.showBackReferences(false);
				Visualizer.vv.repaint();
			}
		});
		menu351.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Configuration.put("directEdges","0");
				if(JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog(null, "Перерисовать?")) {
					Visualizer.repaintVisualization();
				}
			}
		});
		menu352.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Configuration.put("directEdges","1");
				if(JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog(null, "Перерисовать?")) {
					Visualizer.repaintVisualization();
				}
			}
		});
		
		menu31.add(menu311);
		menu31.add(menu312);
		menu34.add(menu341);
		menu34.add(menu342);
		menu35.add(menu351);
		menu35.add(menu352);
		
		menu3.add(menu31);
		menu3.add(menu32);
		menu3.addSeparator();
		menu3.add(menu33);
		menu3.addSeparator();
		menu3.add(menu34);
		menu3.add(menu35);
		
		
		ButtonGroup menu4bg = new ButtonGroup();
		menu4bg.add(menu41);
		menu4bg.add(menu42);
		menu41.setSelected(true);
		menu41.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if( Visualizer.getDSindex() != 0 ) {
					Visualizer.changeVisualization( true, Algorithm.code );
				}
			}
		});
		menu42.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if( Visualizer.getDSindex() != 1 ) {
					Visualizer.changeVisualization( true, Algorithm.code );
				}
			}
		});
		menu4.add(menu41);
		menu4.add(menu42);
		
		add(menu1);
		add(menu2);
		add(menu3);	
		add(menu4);	
	}
}
