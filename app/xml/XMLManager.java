package app.xml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

public class XMLManager {
	private Element root = null;
	
	public XMLManager( Element root ) {
		this.root = root;
	}
	
	public XMLManager( String path ) {
		SAXBuilder builder = new SAXBuilder();
		File xmlFile = new File( path );
		
		try {						
			Document document = (Document) builder.build(xmlFile);
			this.root = document.getRootElement();
		}
		catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (JDOMException jdomex) {
			System.out.println(jdomex.getMessage());
		}
	}
	
	public XMLManager( URL url ) {
		SAXBuilder builder = new SAXBuilder();
		
		try {		
			InputStream in = url.openStream();
			Document document = (Document) builder.build(in);
			this.root = document.getRootElement();
		}
		catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (JDOMException jdomex) {
			System.out.println(jdomex.getMessage());
		}
	}
	
	
	public Element getRoot() {
		return root;
	}
	public ArrayList<Element> search( Element vertex, String name, HashMap<String,String> params, boolean recursive, int count ) {
		if( count == 0 ) {
			return new ArrayList<Element>(); 
		}
		ArrayList<Element> res = new ArrayList<Element>();
		for( Object child: vertex.getChildren() ) {
			Element child_elem = (Element)child;
			if( child_elem.getName().equals(name) ) {
				boolean match = true;
				for( String key : params.keySet() ) {
					if( !params.get(key).equals(child_elem.getAttributeValue(key)) ) {
						match = false;
						break;
					}
				}
				if( match ) {
					res.add(child_elem);
					if( res.size() == count ) {
						return res;
					}
				}
			}
			if( recursive ) {
				res.addAll( search(child_elem, name, params, recursive, count - res.size() ) );
			}
		}
		return res;
	}
	
	public ArrayList<Element> search( String root, String name, HashMap<String,String> params ) {
		return search( this.getRoot().getChild(root), name, params, false, -1 );
	}
	public ArrayList<Element> search( String root, String name, String[] params ) {
		return search( this.getRoot().getChild(root), name, createHashMap(params), false, -1 );
	}
	public ArrayList<Element> search( String root, String name ) {
		return search(root, name, createHashMap(new String[]{}));
	}
	
	public static HashMap<String,String> createHashMap( String[] params ) {
		HashMap<String, String> params_ = new HashMap<String, String>();
		for(int i=0; i<params.length; i+=2) {
			params_.put(params[i],params[i+1]);
		}
		return params_;
	}
	
	public void print( Element root, int t ) {
		String s = "";
		for( int i=0; i<t; i++ ) s += "  ";
		System.out.println(s+root.getName());
		for( Object e : root.getChildren() ) {
			print( (Element)e, t+1 );
			
		}
	}
}
