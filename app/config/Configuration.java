package app.config;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import app.xml.XMLManager;


public class Configuration {
	
	/**
	 * HashMap to store variables
	 */
	private static HashMap<String, String> data = new HashMap<String, String>();
	
	/**
	 * XMLManagers for data in onto
	 */
	public static XMLManager xml_data = null;
	public static XMLManager xml_onto = null;
	
	/**
	 * Context
	 */
//	private static String context = "file:///D:/dev/vhosts/clvis/";
	public static String context = "http://clvis/";
	
	/**
	 * Initialize
	 */
	public static void init() {
		URL url = null;
		
//		XML data
		String data_path = "data.xml";
		try {
			url = new URL(context + data_path);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		xml_data = new XMLManager(url);
		
//		XML onto
		String onto_path = "onto.xml";
		try {
			url = new URL(context + onto_path);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		xml_onto = new XMLManager(url);
	}
	
	public static String get(String key) {
		if( data.containsKey(key) ) {
			return data.get(key);
		}
		return "";
	}
	
	public static String put(String key, String val) {
		return data.put(key, val);
	}
	
//	public static String getFileContents(String path) {
//		URL url = null;
//		try {
//		  url = new URL("file:///D:/dev/vhosts/clvis/data.xml");
//		}
//		catch(MalformedURLException e){}
//		
//		String line;
//		StringBuffer strBuff = new StringBuffer();
//		try {
//			InputStream in = url.openStream();
//			BufferedReader bf = new BufferedReader(new InputStreamReader(in));
//			while ((line = bf.readLine()) != null) {
//				strBuff.append(line);
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return strBuff.toString();
//	}
	
	
}
