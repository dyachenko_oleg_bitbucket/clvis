package app.xml;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.swing.JOptionPane;


import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import app.config.Configuration;
import app.mysql.MySQLConnection;


public class CL2XML {
	public static MySQLConnection db = null;
	
	public static void createXML( String path, boolean isData ) throws SQLException, IOException
	{
		try {
			db = new MySQLConnection(
				Configuration.get("dbhost"), 
				Configuration.get("dbname"), 
				Configuration.get("dbuser"), 
				Configuration.get("dbpass")
			);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error: not connected to database");
			return;
		}
		
		Element root = new Element( isData ? "data" : "onto" );
		Document doc = new Document(root);
		doc.setRootElement(root);
		
		if( isData ) {
			Element root_obj = new Element("objects");
			Element obj = new Element("object");
			String curr_id = "-1";
			ResultSet rs = db.query(
				"SELECT " +
				"	do.id, " +
				"	do.id_ent, " +
				"	doa.id_attr, " +
				"	( " +
				"		CASE WHEN doa.id_dom_val IS NULL THEN doa.value " +
				"		ELSE (SELECT odv.value FROM `ontology_dom_values` AS odv WHERE odv.id = doa.id_dom_val LIMIT 1) " +
				"		END " +
				"	) AS value " +
				"FROM " +
				"	`data_objects` AS do " +
				"INNER JOIN " +
				"	`data_obj_attributes` AS doa " +
				"ON do.id = doa.id_obj " +
				"ORDER BY do.id ASC");
			while( rs.next() ) {
				if( !rs.getString("id").equals(curr_id) ) {
					if( !curr_id.equals("-1") ) {
						root_obj.addContent(obj);
					}
					curr_id = rs.getString("id");
					obj = new Element("object");
					obj.setAttribute("id", curr_id);
					obj.setAttribute("id_ent", rs.getString("id_ent"));
				}
				Element attr = new Element("attribute");
				attr.setAttribute("id", rs.getString("id_attr"));
				try{
					attr.setAttribute("value", rs.getString("value") == null ? "" : rs.getString("value") );
				} catch( Exception ex ) {
					System.out.println( rs.getString("value") );
				}
				obj.addContent(attr);
			}
			root_obj.addContent(obj);
			
			Element root_rel = new Element("relations");
			Element rel = new Element("relation");
			curr_id = "-1";
			rs = db.query(
				"SELECT " +
				"	dr.id, " +
				"	dr.id_rel, " +
				"	dr.id_obj1, " +
				"	dr.id_obj2, " +
				"	dra.id_attr, " +
				"	( " +
				"		CASE WHEN dra.id_dom_val IS NULL THEN dra.value " +
				"		ELSE (SELECT odv.value FROM `ontology_dom_values` AS odv WHERE odv.id = dra.id_dom_val LIMIT 1) " +
				"		END " +
				"	) AS value " +
				"FROM " +
				"	data_relations AS dr " +
				"LEFT JOIN " +
				"	data_rel_attributes AS dra " +
				"ON dr.id_rel = dra.id_rel " +
				"ORDER BY dr.id ASC");
			while( rs.next() ) {
				if( !rs.getString("id").equals(curr_id) && !curr_id.equals("") ) {
					root_rel.addContent(rel);
					curr_id = rs.getString("id");
					rel = new Element("relation");
					rel.setAttribute("id", curr_id);
					rel.setAttribute("id_entity", rs.getString("id_rel"));
					rel.setAttribute("from", rs.getString("id_obj1"));
					rel.setAttribute("to", rs.getString("id_obj2"));
				}
				if( rs.getString("id_attr") != null ) {
					Element attr = new Element("attribute");
					attr.setAttribute("id", rs.getString("id_attr"));
					attr.setAttribute("value", rs.getString("value") == null ? "" : rs.getString("value") );
					//attr.setAttribute("domain_value", rs.getString("id_dom_val") == null ? "" : rs.getString("id_dom_val") );
					rel.addContent(attr);
				}
			}
			root_rel.addContent(rel);
			
			Element root_attr = new Element("attributes");
			rs = db.query(
				"SELECT " +
				"	id, " +
				"	descr, " +
				"	id_ent " +
				"FROM ontology_attributes");
			while( rs.next() ) {
				Element attr = new Element("attribute");
				attr.setAttribute("id", rs.getString("id"));
				attr.setAttribute("name", rs.getString("descr"));
				attr.setAttribute("id_entity", rs.getString("id_ent") );
				root_attr.addContent(attr);
			}
			
			Element root_ent = new Element("entities");
			rs = db.query(
				"SELECT " +
				"	id, " +
				"	descr, " +
				"	type, " +
				"	id_parent " +
				"FROM ontology_entities");
			while( rs.next() ) {
				Element attr = new Element("entity");
				attr.setAttribute("id", rs.getString("id"));
				attr.setAttribute("name", rs.getString("descr"));
				attr.setAttribute("type", rs.getString("type") );
				attr.setAttribute("parent_entity", rs.getString("id_parent") == null ? "" : rs.getString("id_parent") );
				// short and full link
				ResultSet rs1 = db.query( 
					"SELECT id_attr, id_rel " +
					"FROM ontology_settings " +
					"WHERE " +
					"	id_ent = '" + rs.getString("id") + "' AND " +
					"	short_link IS NOT NULL " +
					"ORDER BY " +
					"	id_ent ASC, " +
					"	short_link ASC"
				);
				String short_link = "";
				while(rs1.next()) {
					if( !short_link.equals("") ) {
						short_link += " ";
					}
					short_link += rs1.getString("id_rel") != null ? 
						"r"+rs1.getString("id_rel") : "a"+rs1.getString("id_attr");
				}
				attr.setAttribute("short_link", short_link );
				root_ent.addContent(attr);
			}
			
			doc.getRootElement().addContent(root_obj);
			doc.getRootElement().addContent(root_rel);
			doc.getRootElement().addContent(root_attr);
			doc.getRootElement().addContent(root_ent);
		} else {
			Element root_ent = new Element("entities");
			Element ent = new Element("entity");
			String curr_id = "-1";
			ResultSet rs = db.query(
				"SELECT " +
				"	oe.id, " +
				"	oe.descr, " +
				"	oe.id_parent, " +
				"	COUNT(*) AS count " +
				"FROM " +
				"	ontology_entities AS oe " +
				"LEFT JOIN " +
				"	data_objects AS do " +
				"ON do.id_ent = oe.id " +
				"WHERE " +
				"	oe.type = 1 " +
				"GROUP BY oe.id"); 
			while( rs.next() ) {
				if( !rs.getString("id").equals(curr_id) ) {
					if( !curr_id.equals("-1") ) {
						root_ent.addContent(ent);
					}
					curr_id = rs.getString("id");
					ent = new Element("entity");
					ent.setAttribute("id", curr_id);
					ent.setAttribute("count", rs.getString("count") );
					ent.setAttribute("parent", rs.getString("id_parent") == null ? "" : rs.getString("id_parent") );
					ent.setAttribute("name", rs.getString("descr"));
				}
			}
			root_ent.addContent(ent);
			
			Element root_rel = new Element("relations");
			Element rel = new Element("relation");
			curr_id = "-1";
			rs = db.query(
				"SELECT " +
				"	orel.*, " +
				"	oe.descr " +
				"FROM " +
				"	ontology_relations AS orel, " +
				"	ontology_entities AS oe " +
				"WHERE oe.id = orel.id_rel");
			while( rs.next() ) {
				if( !rs.getString("id_rel").equals(curr_id) ) {
					if( !curr_id.equals("-1") ) {
						root_rel.addContent(rel);
					}
					curr_id = rs.getString("id_rel");
					rel = new Element("relation");
					rel.setAttribute("id", curr_id);
					rel.setAttribute("name", rs.getString("descr"));
					rel.setAttribute("type", "2");
					rel.setAttribute("from", rs.getString("id_ent1"));
					rel.setAttribute("to", rs.getString("id_ent2"));
				}
			}
			root_rel.addContent(rel);
			
			rs = db.query(
				"SELECT " +
				"	oe.id, " +
				"	oe.descr, " +
				"	oe.id_parent, " +
				"	COUNT(*) AS count " +
				"FROM " +
				"	ontology_entities AS oe " +
				"LEFT JOIN " +
				"	data_objects AS do " +
				"ON do.id_ent = oe.id " +
				"WHERE " +
				"	oe.type = 1 " +
				"GROUP BY oe.id"); 
			while( rs.next() ) {
				if( rs.getString("id_parent") == null ) continue;
				rel = new Element("relation");
				curr_id = rs.getString("id");
				rel.setAttribute("id", rs.getString("id_parent")+">"+curr_id);
				rel.setAttribute("type", "2");
				rel.setAttribute("name", "Класс-подкласс");
				rel.setAttribute("from", curr_id);
				rel.setAttribute("to", rs.getString("id_parent"));
				root_rel.addContent(rel);
			}
			
			doc.getRootElement().addContent(root_ent);
			doc.getRootElement().addContent(root_rel);
		}
		
		XMLOutputter xmlOutput = new XMLOutputter();
		xmlOutput.setFormat(Format.getPrettyFormat());
		xmlOutput.output(doc, new FileWriter(path));
	}
}
