package app.gui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class ColorColumnRenderer extends DefaultTableCellRenderer 
{
   public ColorColumnRenderer() {
      super(); 
   }

   public Component getTableCellRendererComponent
        (JTable table, Object value, boolean isSelected,
         boolean hasFocus, int row, int col) 
   {
      Component cell = super.getTableCellRendererComponent
         (table, value, isSelected, hasFocus, row, col);

      Color color = (Color)table.getModel().getValueAt(row, col);
      cell.setBackground( color );
      cell.setForeground( color );

      return cell;
   }
}