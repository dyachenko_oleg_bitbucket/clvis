package app.gui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class TableRenderer {
	public final static TableCellRenderer val = new TableCellRenderer() {
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			try{
				String text = (String)table.getValueAt(row,column);
            	JLabel label_cl = new JLabel(text);
            	label_cl.setBackground(Color.WHITE);
	            label_cl.setOpaque(true);
	            label_cl.setToolTipText((text.length()>150) ? text.substring(0,150)+"..." : text);
	            return (text.equals(""))? null : label_cl;
            }
            catch(Exception e){return null;}
		}
	};
}
