package app.gui;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

public class AbstractModel extends AbstractTableModel implements AbstractModelInterface{
	private static final long serialVersionUID = 1L;

	private Vector<String> columnNames;
	@SuppressWarnings("rawtypes")
	private Vector<Class> columnTypes;
	private Vector<Vector<Object>> tableData;
	
	public AbstractModel(Vector<String> columnNames, @SuppressWarnings("rawtypes") Vector<Class> columnTypes) throws Exception {
		if( columnNames.size() != columnTypes.size() ) {
			throw new Exception("Bad table model initialization.\n");
		}
		this.columnNames = columnNames;
		this.columnTypes = columnTypes;
		tableData = new Vector<Vector<Object>>();
	}

	public int getColumnCount() {
		return columnNames.size();
	}
	public int getRowCount() {
		return tableData.size();
	}
	public Object getValueAt(int row, int column) {
		return tableData.get(row).get(column);
	}
	public String getColumnName(int column) {
		return columnNames.get(column);
	}
	public void setValueAt(Object value, int row, int col){
		tableData.get(row).set(col, value);
		fireTableCellUpdated(row, col);
	}
	public boolean isCellEditable(int row, int col){
		return false;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Class getColumnClass(int col){
		try{
			return columnTypes.get(col);
		}
		catch(Exception e){return null;}
		
	}

	public void addRow(Vector<Object> row) throws Exception {
		if( row.size() != columnNames.size() ) {
			throw new Exception("Bad size of added row.\n");
		}
		tableData.add(row);
		fireTableDataChanged();
	}

	public void clear() {
		tableData = new Vector<Vector<Object>>();
		fireTableDataChanged();
	}

	public void putContents(Vector<Vector<Object>> data) {
		tableData = data;
		fireTableDataChanged();
	}

	public Vector<Vector<Object>> getContents() {
		return tableData;
	}
	
}
