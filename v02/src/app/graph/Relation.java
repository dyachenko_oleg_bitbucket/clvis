package app.graph;

import java.util.ArrayList;
import java.util.Vector;


public class Relation implements RelationInterface {
	private String id = "";
	private String entity = ""; 
	
	public String getId() {
		return id;
	}
	public String setId(String id) {
		this.id = id;
		return this.id;
	}
	public String getEntity() {
		return entity;
	}
	
	public ArrayList<Vector<String>> info() {
		return null;
	}

	public Relation( String id ) {
		this.id = id;
	}
	public Relation( String id, String ent_id ) {
		this.id = id;
		this.entity = ent_id;
	}
}
