package app.graph;

import java.util.ArrayList;
import java.util.HashMap;

import edu.uci.ics.jung.graph.util.Pair;

public class TreeNode {
	public Point pos = new Point(0,0); 
	
	private TreeNode parent = null;
	private Node node = null;
	private ArrayList<TreeEdge> children = new ArrayList<TreeEdge>();
	private int level = 0;	
	
	public String toString() {
		return "#"+getId();
	}
	
	
	public void flush() {
		setParent(null);
		setLevel(0);
		//pos = new Point(0,0);
		children.clear();
	}	
	public TreeNode getRoot() {
		if( parent == null ) return this;
		return parent.getRoot();
	}
	public void clean() {
		getRoot().clearLevels();
		getRoot().clearParents();
	}
	public void clearParents() {
		this.parent = null;
		for( TreeNode tn : getChildren() ) {
			tn.clearParents();
		}
	}
	
	
	public TreeNode getParent() {
		return parent;
	}
	public TreeNode setParent(TreeNode parent) {
		this.parent = parent;
		return this.parent;
	}
	public boolean hasParent( TreeNode treeNode ) {
		if( getParent() == null ) return false;
		return getParent().equals(treeNode);
	}
	public Node getNode() {
		return node;
	}
	public Node setNode(Node node) {
		this.node = node;
		return this.node;
	}
	public int getLevel() {
		return level;
	}
	public int setLevel(int level) {
		this.level = level;
		return this.level;
	}
	public String getId() {
		return getNode().getId();
	}
	public String getEntity() {
		return getNode().getEntity();
	}
	
	
	/*
	 * Main methods
	 */
	public ArrayList<TreeEdge> getEdges() {
		return children;		
	}
	public ArrayList<Pair<TreeNode>> getAllEdges() {
		ArrayList<Pair<TreeNode>> edges = new ArrayList<Pair<TreeNode>>();
		for( TreeEdge edge : getEdges() ) {
			edges.add( new Pair<TreeNode>(this, edge.getTreeNode()) );
		}
		for( TreeNode tn: getChildren() ) {
			edges.addAll(tn.getAllEdges());
		}
		return edges;		
	}
	public HashMap<TreeEdge, TreeNode> getAllTreeEdges() {
		HashMap<TreeEdge, TreeNode> edges = new HashMap<TreeEdge, TreeNode>();
		for( TreeEdge edge : getEdges() ) {
			edges.put( edge, this );
		}
		for( TreeNode tn: getChildren() ) {
			edges.putAll(tn.getAllTreeEdges());
		}
		return edges;		
	}
	public ArrayList<TreeNode> getAllNodes() {
		ArrayList<TreeNode> nodes = new ArrayList<TreeNode>();
		nodes.add(this);
		for( TreeNode tn: getChildren() ) {
			nodes.addAll(tn.getAllNodes());
		}
		return nodes;		
	}
	public void clearLevels() {
		this.setLevel(0);
		ArrayList<TreeEdge> edges = getEdges();
		for( TreeEdge treeEdge : edges ) {
			if( treeEdge.getTreeNode().getLevel() != 0 ) {
				treeEdge.getTreeNode().clearLevels();
			}
		}
	}
	public void setLevels() {
		setLevel(1);
		ArrayList<TreeNode> wave = new ArrayList<TreeNode>();
		ArrayList<TreeNode> wave_tmp = new ArrayList<TreeNode>();
		wave.add(this);
		while( !wave.isEmpty() ) {
			wave_tmp = new ArrayList<TreeNode>();
			for( TreeNode tn : wave ) {
				for( TreeEdge te : tn.getEdges() ) {
					if( te.getTreeNode().getLevel() == 0 ) {
						te.getTreeNode().setLevel(tn.getLevel()+1);
						te.getTreeNode().setParent(tn);
						wave_tmp.add( te.getTreeNode() );
					}
				}
			}
			wave = wave_tmp;
		}
	}
	public ArrayList<TreeNode> getChildren() {
		ArrayList<TreeNode> childs = new ArrayList<TreeNode>();
		for( TreeEdge treeEdge : children ) {
			if( treeEdge.getTreeNode().hasParent(this) && !childs.contains(treeEdge.getTreeNode()) ) {	
				childs.add(treeEdge.getTreeNode());
			}
		}
		return childs;		
	}
	
	public int getHeight() {
		ArrayList<TreeNode> childs = getChildren();
		if( childs.isEmpty() ) return 1;
		int level = 0;
		int height = 0;
		for( TreeNode tn : childs ) {
			height = tn.getHeight();
			if( height > level ) {
				level = height;
			}
		}
		return level + 1;
	}
	public int countLeaves() {
		int count = 0;
		ArrayList<TreeNode> childs = getChildren();
		if( childs.isEmpty() ) return 1;
		for( TreeNode tn : childs ) {
			count += tn.countLeaves();
		}
		return count;
	}
	public int countElements() {
		ArrayList<TreeNode> childs = getChildren();
		if( childs.isEmpty() ) return 1;
		int count = 1;
		for( TreeNode tn : childs ) {
			count += tn.countElements();
		}
		return count;
	}
	
	public TreeEdge addChildren(TreeNode treeNode, RelationPair relationPair ) {
		for( TreeEdge treeEdge : children ) {
			if( treeEdge.getTreeNode().equals(treeNode) && treeEdge.getRelationPair().equals(relationPair) ) {
				return treeEdge;
			}
		}
		TreeEdge treeEdge = new TreeEdge(treeNode, relationPair);
		this.children.add(treeEdge);
		return treeEdge;
	}
	public ArrayList<TreeEdge> setChildren(ArrayList<TreeEdge> children) {
		this.children = children;
		return this.children;
	}
	
	
	// Constructor	
	public TreeNode( Node node ) {
		setNode(node);
	}
		
	public void print() {
		for(int i=0; i<getLevel()-1; i++) System.out.print("  ");
		System.out.println(getId() + " : " + pos);
		ArrayList<TreeNode> childs = getChildren();
		for( TreeNode tn : childs ) {
			tn.print();
		}
	}
	
	
	
}
