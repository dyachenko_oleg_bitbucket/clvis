package app.graph;

public class Point3 extends Point {
	public double z = 0;
	
	public Point3 movex( double x ) {
		this.x += x;
		return this;
	}
	public Point3 movey( double y ) {
		this.y += y;
		return this;
	}
	public Point3 movez( double z ) {
		this.z += z;
		return this;
	}
	public Point3 move( Point3 pt ) {
		return this.movex(pt.x).movey(pt.y).movez(pt.z);
	}
	public Point3 mult( double multiplier ) {
		return this.mult(multiplier).setz(multiplier*z);
	}
	public Point3 minus() {
		return this.minus().setz(-z);
	}	
	public double abs() {
		return Math.sqrt( x*x + y*y + z*z );
	}
	
	public Point3 setx( double x ) {
		this.x = x;
		return this;
	}
	public Point3 sety( double y ) {
		this.y = y;
		return this;
	}
	public Point3 setz( double z ) {
		this.z = z;
		return this;
	}
	public Point3 set( Point3 pt ) {
		return this.setx(pt.x).sety(pt.y).setz(pt.z);
	}
		
	public String toString() {
		return "[" + x + "; " + y + "; " + z + "]";
	}
	
	public Point toPoint() {
		return new Point(x,y);
	}
	public Point3(double x, double y, double z) {
		super(x, y);
		this.z = z;
	}
	public Point3(Point3 pt) {
		super(pt);
		this.z = pt.z;
	}
	public Point3 clone() {
		return new Point3(this);
	}
	public static Point3 max(Point3 pt1, Point3 pt2) {
		return new Point3( 
			pt1.x>=pt2.x ? pt1.x : pt2.x, 
			pt1.y>=pt2.y ? pt1.y : pt2.y,
			pt1.z>=pt2.z ? pt1.z : pt2.z
		);
	}
	public static Point3 min(Point3 pt1, Point3 pt2) {
		return new Point3( 
			pt1.x<=pt2.x ? pt1.x : pt2.x, 
			pt1.y<=pt2.y ? pt1.y : pt2.y,
			pt1.z<=pt2.z ? pt1.z : pt2.z 
		);
	}
	
}
