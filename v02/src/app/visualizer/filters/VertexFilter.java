package app.visualizer.filters;

import org.apache.commons.collections15.Predicate;

import app.graph.Relation;
import app.graph.TreeNode;
import app.visualizer.Visualizer;
import app.visualizer.colorers.AnalysisColorer;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.Context;

public class VertexFilter implements Predicate<Context<Graph<TreeNode,Relation>,TreeNode>> {
	protected boolean on;
	protected final static int MIN_DEGREE = 4;

	public VertexFilter(boolean on) {
		this.on = on;
	}

	public void toggle() {
		on = !on;
	}

	public boolean evaluate(Context<Graph<TreeNode,Relation>,TreeNode> context) {
		Graph<TreeNode,Relation> graph = context.graph;
		TreeNode v = context.element;
		
		if( AnalysisColorer.state() ) {
			return true;
		}
		
		if( !on ) return true;
		return Visualizer.getDataset().getVertexStatus(v);		
	}
}
