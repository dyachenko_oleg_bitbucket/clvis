package app.visualizer.layout;


import java.awt.geom.Point2D;

import org.apache.commons.collections15.Transformer;

import app.graph.TreeNode;

public class VisualizerPixelTransformer implements Transformer<TreeNode,Point2D> {

	public static int resize = 50;
	
	@Override
	public Point2D transform(TreeNode arg0) {
		return new Point2D.Double(
			Math.round(resize+resize*arg0.pos.x),
			Math.round(resize+resize*arg0.pos.y)
		);
	}
	
}
