package app.visualizer.info;


import java.util.ArrayList;

import org.apache.commons.collections15.Transformer;

import app.graph.TreeNode;
import app.visualizer.Visualizer;


public class VertexLabeler implements Transformer<TreeNode,String> {

	@Override
	public String transform(TreeNode tn) {
		return Visualizer.getDataset().getVertexLabel(tn);
	}
}