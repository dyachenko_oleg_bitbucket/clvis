package app.layout;

/*
 * Created on Jul 21, 2005
 *
 * Copyright (c) 2005, the JUNG Project and the Regents of the University 
 * of California
 * All rights reserved.
 *
 * This software is open-source under the BSD license; see either
 * "license.txt" or
 * http://jung.sourceforge.net/license.txt for a description.
 */

import java.awt.Dimension;
import java.awt.geom.Point2D;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.graph.Graph;

/**
 * StaticLayout places the vertices in the locations specified by its Transformer<V,Point2D>
 * initializer. Vertex locations can be placed in a Map<V,Point2D> and then supplied to
 * this layout as follows:
 * <code>
            Transformer<V,Point2D> vertexLocations =
        	TransformerUtils.mapTransformer(map);
 * </code>
 * @author Tom Nelson - tomnelson@dev.java.net
 *
 * @param <V>
 * @param <E>
 */
public class FixedLayout<V, E> extends AbstractFixedLayout<V,E> {
	
	
    public FixedLayout(Graph<V,E> graph, Transformer<V,Point2D> initializer, Dimension size) {
        super(graph, initializer, size);
    }
    public FixedLayout(Graph<V,E> graph, Transformer<V,Point2D> initializer) {
        super(graph, initializer);
    }
    public FixedLayout(Graph<V,E> graph) {
    	super(graph);
    }
    public FixedLayout(Graph<V,E> graph, Dimension size) {
    	super(graph, size);
    }
    public void initialize() {}
    public void reset() {}
    
    

}
