package app.algorithm;


import java.util.ArrayList;
import java.util.HashMap;

import app.graph.Point;
import app.graph.Point3;
import app.graph.TreeNode;


public class CircleLayout extends Algorithm {
	// alpha innerRadius outerRadius 
	HashMap<TreeNode, Point3> data = new HashMap<TreeNode, Point3>();
	
	double node = 0.1;
	double dist = 0.2;
		
	@Override
	public void setCoordinates(TreeNode root) {
		root.setLevels();
		initDataset(root);
		setCoordinatesStep(root);
		setAbsPositions(root);
	}
	@Override
	public void setCoordinates(ArrayList<TreeNode> list) {
		for( TreeNode root : list ) {
			setCoordinates(root);
		}
		// TODO Think of cool layout for CCs
		placeComponents(list);
		/*
		for( int i=1; i<list.size(); i++ ) {
			move(list.get(i), new Point( max(list.get(i-1)).x + list.get(i).pos.x - min(list.get(i)).x + 2 , 0));
		}
		*/
	}	
	
	private void setCoordinatesStep( TreeNode root ) {
		ArrayList<TreeNode> childs = root.getChildren();
		if( childs.isEmpty() ) {
			data.get(root).setx(0).sety(node).setz(0);
		} else {
			for( TreeNode tn : childs ) {
				setCoordinatesStep(tn);
			}
			
			// base circle radius
			double d = 0; 
			for( TreeNode tn : childs ) {
				d = d < data.get(tn).y ? data.get(tn).y : d; 
			}
			// fix angles ~ PI
			double angleSum = 0;
			for( TreeNode tn : childs ) {
				angleSum += Math.atan( data.get(tn).y/(d+data.get(tn).y) ); 
			}
			double angleMult = Math.PI/angleSum;
			
			// computing 
			double r1 = data.get(childs.get(0)).y;
			double angle = -Math.atan(r1/(d+r1));
			data.get(childs.get(0)).setx(angle*angleMult).setz(d+r1);
			for( int i=1; i<childs.size(); i++ ) {
				angle += Math.atan(data.get(childs.get(i-1)).y/(d+data.get(childs.get(i-1)).y));
				angle += Math.atan(data.get(childs.get(i)).y/(d+data.get(childs.get(i)).y));
				data.get(childs.get(i))
					.setx(angle*angleMult)
				//	.sety(0)
					.setz(d+data.get(childs.get(i)).y);
			}
			// inner radius for root
			data.get(root).sety(3*d);
			
			
			
			/*
			double maxR = 0;
			for( TreeNode tn : childs ) {
				maxR = maxR < data.get(tn).y ? data.get(tn).y : maxR; 
			}
			double alpha = Math.PI / childs.size();
			double R = 0;
			
			switch( childs.size() ) {
			case 1:
				R = dist;
			break;
			case 2:
				R = dist;
			break;
			default:
				R = maxR / Math.atan(alpha);
			break;
			}
			
			for( int i=0; i<childs.size(); i++ ) {
				data.get(childs.get(i))
					.setx((2*i+1)*alpha)
					//.sety(maxR+dist)
					.setz( dist + data.get(childs.get(i)).y / Math.atan(alpha));
			}
			// inner radius for root
			data.get(root).sety(maxR+R);
			*/
		}
	}
	
	// init
	private void initDataset( TreeNode root ) {
		data.put(root, new Point3(0,0,0));
		for( TreeNode tn : root.getChildren() ) {
			initDataset(tn);
		}
	}
	private void setAbsPositions( TreeNode root ) {
		root.pos.set( new Point(0,0) );
		setAbsPositionsStep(root, 0);
	}
	
	private void setAbsPositionsStep(TreeNode root, double incAngle) {
		for( TreeNode tn : root.getChildren() ) {
			tn.pos
				.setx( root.pos.x + data.get(tn).z * Math.cos(incAngle + data.get(tn).x  - Math.PI ) )
				.sety( root.pos.y + data.get(tn).z * Math.sin(incAngle + data.get(tn).x  - Math.PI ) );
			setAbsPositionsStep(tn, incAngle + data.get(tn).x - Math.PI);
		}
	}
}