package app.algorithm;

import edu.uci.ics.jung.graph.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;

import app.graph.Point;
import app.graph.TreeNode;

public class KamadaKawaiLayout extends Algorithm {
	
	HashMap<TreeNode, ArrayList<TreeNode>> nodes = new HashMap<TreeNode, ArrayList<TreeNode>>();
	HashMap<String, Integer> dists = new HashMap<String, Integer>();
	
	double l = 1;
	double k = 1;
	int stepCount = 100;
	double eps = 0.01;
	
	@Override
	public void setCoordinates(TreeNode root) {
		root.setLevels();
		if( !nodes.containsKey(root) ) {
			nodes.put(root,root.getAllNodes());
		}
		initDistances(root);
		for( int i=1; i<=stepCount; i++ ) {
			try{
				setCoordinatesStep(root);
			} catch(Exception e) {
				break;
			}
		}
		
	}
	@Override
	public void setCoordinates(ArrayList<TreeNode> list) {
		for( TreeNode root : list ) {
			setCoordinates(root);
		}
		// TODO Think of cool layout for CCs
		placeComponents(list);
	}	
	
	private void setCoordinatesStep( TreeNode root ) throws Exception {
		ArrayList<TreeNode> _nodes = nodes.get(root);
		HashMap<TreeNode, Double> dx = new HashMap<TreeNode, Double>();
		HashMap<TreeNode, Double> dy = new HashMap<TreeNode, Double>();
		for( int m=0; m<_nodes.size(); m++ ) {
			// dx
			double _dx = 0;
			double _dy = 0;
			for( int j=0; j<_nodes.size(); j++ ) {
				if(j==m) continue;
				double dij = dists.get(_nodes.get(m)+" "+_nodes.get(j));
				double kij = k*Math.pow(dij, -2); 
				double lij = l*dij;
				Point dpos = _nodes.get(m).pos.clone().move(_nodes.get(j).pos.clone().minus());
				_dx += kij * dpos.x * ( 1 - lij/dpos.abs());
				_dy += kij * dpos.y * ( 1 - lij/dpos.abs());
			}
			dx.put(_nodes.get(m), _dx);
			dy.put(_nodes.get(m), _dy);
		}
		int m = 0;
		double max = 0;
		for( int i=0; i<_nodes.size(); i++ ) {
			TreeNode v = _nodes.get(i); 
			double _d = dx.get(v)*dx.get(v) + dy.get(v)*dy.get(v);
			if( _d > max ) {
				max = _d;
				m = i;
			}
		}
		if( max > eps ) {
			TreeNode v = _nodes.get(m); 
			double dxx = 0;
			double dxy = 0;
			double dyx = 0;
			double dyy = 0;
			for( int j=0; j<_nodes.size(); j++ ) {
				if(j==m) continue;
				double dij = dists.get(_nodes.get(m)+" "+_nodes.get(j));
				double kij = k*Math.pow(dij, -2); 
				double lij = l*dij;
				Point dpos = _nodes.get(m).pos.clone().move(_nodes.get(j).pos.clone().minus());
				double dposabs_3 = Math.pow(dpos.abs(),-3); 
				//dxx += kij * ( 1 - lij/dpos.abs());
				dxx += kij * ( 1 - dpos.y*dpos.y*lij*dposabs_3);
				dyy += kij * ( 1 - dpos.x*dpos.x*lij*dposabs_3);
				dxy += kij * dpos.x * dpos.y*lij*dposabs_3;
			}
			dyx = dxy; 	
			double d = dxx*dyy - dxy*dxy;
			d = 1/d;
			double delta_x = d*( dy.get(v)*dxy - dx.get(v)*dyy );
			double delta_y = d*( dx.get(v)*dyx - dy.get(v)*dxx );
			v.pos.move( new Point(delta_x,delta_y) );
		}
		
		
	}
	private void initDistances( TreeNode root ) { 
		ArrayList<TreeNode> _nodes = nodes.get(root);
		ArrayList<Pair<TreeNode>> _edges = root.getAllEdges();
		ArrayList<TreeNode> _nodes_gone = new ArrayList<TreeNode>();
		ArrayList<TreeNode> _nodes_wave = new ArrayList<TreeNode>();
		ArrayList<TreeNode> _nodes_wave_new = new ArrayList<TreeNode>();
		for( TreeNode v: _nodes ) {
			for( TreeNode u: _nodes ) {
				dists.put(v+" "+u, _nodes.size()-1);
			}
			dists.put(v+" "+v, 0);
		}
		
		for( int i=0; i<_nodes.size(); i++ ) {
			TreeNode v = _nodes.get(i);
			_nodes_gone = new ArrayList<TreeNode>();
			_nodes_wave = new ArrayList<TreeNode>();
			for( int j=0; j<=i; j++ ) {
				_nodes_gone.add(_nodes.get(j));
			}
			_nodes_wave.add(v);
			
			while( !_nodes_wave.isEmpty() ){
				_nodes_wave_new = new ArrayList<TreeNode>();
				for( Pair<TreeNode> edge : _edges ) {
					if( _nodes_wave.contains(edge.getFirst()) && !_nodes_gone.contains(edge.getSecond()) ) {
						_nodes_wave_new.add( edge.getSecond() );
						int newval = dists.get(v+" "+edge.getFirst()) + 1;
						if( newval < dists.get(v+" "+edge.getSecond()) ) {
							dists.put(v+" "+edge.getSecond(), newval);
							dists.put(edge.getSecond()+" "+v, newval);
						}
					}
				}
				_nodes_wave = _nodes_wave_new;
				_nodes_gone.addAll(_nodes_wave);
			}
		}
		
		/*
		for( TreeNode v: _nodes ) {
			if( !v.equals(root) ) {
				for( TreeNode u: _nodes ) {
					if( !v.equals(u) ) {
						int rv = dists.get(root+" "+v);
						int ru = dists.get(root+" "+u);
						dists.put(v+" "+u, Math.abs(rv-ru));
					}
				}
			}
		}
		*/
	}
}
