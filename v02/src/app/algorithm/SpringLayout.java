package app.algorithm;


import java.util.ArrayList;
import java.util.HashMap;

import app.graph.Point;
import app.graph.TreeEdge;
import app.graph.TreeNode;

public class SpringLayout extends Algorithm {
	
	private int stepCount = 100; 
	double crep = 1;
	double cspring = 2;
	double delta = 0.1;
	double l = 1;
			
	
	// force 
	HashMap<TreeNode, ArrayList<TreeNode>> nodes = new HashMap<TreeNode, ArrayList<TreeNode>>();
	
	@Override
	public void setCoordinates(TreeNode root) {
		root.setLevels();
		if( !nodes.containsKey(root) ) {
			nodes.put(root,root.getAllNodes());
		}
		for( int i=0; i<stepCount; i++ ) {
			//System.out.println(i+1);
			setCoordinatesStep(root);
		}
	}
	@Override
	public void setCoordinates(ArrayList<TreeNode> list) {
		for( TreeNode root : list ) {
			setCoordinates(root);
		}
		// TODO Think of cool layout for CCs
		placeComponents(list);
	}	
	
	private void setCoordinatesStep( TreeNode root ) {
		ArrayList<TreeNode> _nodes = nodes.get(root);
		//ArrayList<Pair<TreeNode>> _edges = edges.get(root);
		HashMap<TreeNode, Point> forces = new HashMap<TreeNode, Point>();
		for( TreeNode v: _nodes ) {
			forces.put(v, new Point(0,0));
			for( TreeNode u: _nodes ) {
				if( !u.equals(v) ) {
					boolean pairExist = false;
					Point up = u.pos.clone();
					Point vp = v.pos.clone();
					Point force = vp.move(up.minus());
					double dist = force.abs(); 
					force.mult(1/dist);
					for( TreeEdge te : u.getEdges() ) {
					//for(Pair<TreeNode> pair : _edges) {
						//if( pair.getFirst().equals(u) && pair.getSecond().equals(v)	) {
						if( te.getTreeNode().equals(v) ) {
							// spring
							pairExist = true;
							force.mult(-cspring*Math.log(dist/l));
							break;
						}
					}
					if( !pairExist ) {
						// rep
						force.mult(crep*Math.pow(dist,-2));
					}
					forces.get(v).move(force);
				}
			}
		}
		for( TreeNode v: _nodes ) {
			v.pos.move( forces.get(v).mult(delta) );
		}
	}
	
}
