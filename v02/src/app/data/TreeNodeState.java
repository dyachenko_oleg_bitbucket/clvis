package app.data;

public enum TreeNodeState {
	OFF,
	HIDE,
	SHOW;
	
	private static String[] strings = new String[]{
		"Удалить",
		"Скрыть",
		"Показать"
	};
	
	public boolean isOFF() {
		return this == OFF;
	}
	public boolean isShown() {
		return this == SHOW;
	}
	public boolean isHidden() {
		return this != SHOW;
	}
	
	public TreeNodeState min(TreeNodeState t1, TreeNodeState t2) {
		return t1.ordinal() > t2.ordinal() ? t2 : t1; 
	}
	public TreeNodeState max(TreeNodeState t1, TreeNodeState t2) {
		return t1.ordinal() < t2.ordinal() ? t2 : t1; 
	}
	
	public TreeNodeState next() {
		return TreeNodeState.values()[(ordinal()+1)%values().length];		
	}
	
	public String toString() {
		return strings[ordinal()];
	}
	public static TreeNodeState fromString(String str) {
		for( int i=0; i<values().length; i++ ) {
			if( strings[i].equals(str) ) {
				return values()[i];
			} 	
		}
		return TreeNodeState.OFF;
	} 
}
