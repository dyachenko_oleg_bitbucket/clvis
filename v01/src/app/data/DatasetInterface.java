package app.data;


import java.util.ArrayList;
import java.util.Map;

import app.graph.Node;
import app.graph.Relation;
import app.graph.TreeNode;
import app.gui.Table;
import app.visualizer.colorers.EdgeColorer;
import app.visualizer.colorers.VertexColorer;
import app.xml.XMLManager;


public interface DatasetInterface {
	public void setXML(String path);
	public XMLManager getXML();
	
	public VertexColorer getVertexColorer();
	public EdgeColorer getEdgeColorer();
	
	public void init();
	public Map<String,Node> getNodes();
	public Map<String,Relation> getRelations();
	public Map<String,TreeNode> getTreeNodes();
	
	public void initFilters();
	public void off(String key, int code);
	public void hide(String key, int code);
	public void show(String key, int code);
	public TreeNodeState status(String key, int code);
	
	public void openFilterDialog(int code) throws Exception;
	
	public boolean getVertexStatus(TreeNode root);
	public boolean getEdgeStatus(Relation e);
	
	
	public void applyFilters(String path);
	public XMLManager getXMLf();
	
	public ArrayList<TreeNode> getConnectedComponents(String focusNode);
	public TreeNode getConnectedComponent(String focusNode, ArrayList<String> ids);
	
	/* INFO: vertex */
	public String getVertexLabel( TreeNode root );
	public String getVertexName( TreeNode root );
	public String getVertexFullName( TreeNode root );
	public String getVertexInfo( TreeNode root );
	
	/* INFO: edge */
	public String getEdgeLabel( Relation rel );
	public String getEdgeName( Relation rel );
	public String getEdgeInfo( Relation rel );
	
	/* INFO: data */
	public String getEntityName( String id );
	public String getObjectName( String id );
	public String getRelationName( String id );
}
