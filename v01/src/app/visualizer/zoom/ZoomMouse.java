package app.visualizer.zoom;

import java.awt.event.InputEvent;

import app.graph.Relation;
import app.graph.TreeNode;
import app.visualizer.Visualizer;

import edu.uci.ics.jung.visualization.control.AnimatedPickingGraphMousePlugin;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.PickingGraphMousePlugin;
import edu.uci.ics.jung.visualization.control.RotatingGraphMousePlugin;
import edu.uci.ics.jung.visualization.control.ScalingGraphMousePlugin;
import edu.uci.ics.jung.visualization.control.ShearingGraphMousePlugin;
import edu.uci.ics.jung.visualization.control.TranslatingGraphMousePlugin;

public class ZoomMouse extends DefaultModalGraphMouse<TreeNode, Relation> {
    @Override
    protected void loadPlugins() {
        pickingPlugin = new PickingGraphMousePlugin<TreeNode, Relation>();
        animatedPickingPlugin = new AnimatedPickingGraphMousePlugin<TreeNode, Relation>();
        translatingPlugin = new TranslatingGraphMousePlugin(InputEvent.BUTTON1_MASK);
        scalingPlugin = new ScalingGraphMousePlugin(Visualizer.scaler, 0, in, out);
        rotatingPlugin = new RotatingGraphMousePlugin();
        shearingPlugin = new ShearingGraphMousePlugin();

        add(scalingPlugin);
        setMode(Mode.TRANSFORMING);
    }
}
