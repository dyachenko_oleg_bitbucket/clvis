package app.visualizer.info;


import java.util.ArrayList;

import org.apache.commons.collections15.Transformer;

import app.graph.TreeNode;
import app.visualizer.Visualizer;


public class VertexStringer implements Transformer<TreeNode,String> {

	ArrayList<TreeNode> map = new ArrayList<TreeNode>();
	
	@Override
	public String transform(TreeNode tn) {
		if( !map.contains(tn) ) {
			return "";
		} else {
			return Visualizer.getDataset().getVertexName(tn);
		}
	}
	
	public void put( TreeNode tn ) {
		if( map.contains(tn) ) {
			map.remove(tn);
		} else {
			map.add(tn);
		}
	}
	public void put( TreeNode tn, boolean state ) {
		if( map.contains(tn) ) {
			if(!state) map.remove(tn);
		} else {
			if(state) map.add(tn);
		}
	}
}
