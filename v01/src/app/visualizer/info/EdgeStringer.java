package app.visualizer.info;


import java.util.ArrayList;

import org.apache.commons.collections15.Transformer;

import app.graph.Relation;
import app.visualizer.Visualizer;


public class EdgeStringer implements Transformer<Relation,String> {

	ArrayList<Relation> map = new ArrayList<Relation>();
	
	@Override
	public String transform(Relation rel) {
		if( !map.contains(rel) ) {
			return "";
		} else {
			return Visualizer.getDataset().getEdgeName(rel);
		}
	}
	
	public void put( Relation tn ) {
		if( map.contains(tn) ) {
			map.remove(tn);
		} else {
			map.add(tn);
		}
	}
	public void put( Relation tn, boolean state ) {
		if( map.contains(tn) ) {
			if(!state) map.remove(tn);
		} else {
			if(state) map.add(tn);
		}
	}
}
