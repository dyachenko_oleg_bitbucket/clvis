package app.visualizer.renderers;

import java.awt.Color;
import java.awt.Paint;

import org.apache.commons.collections15.Transformer;

import app.graph.Relation;
import app.graph.TreeNode;
import app.visualizer.Visualizer;
import app.visualizer.colorers.AnalysisColorer;
import app.visualizer.colorers.EdgeColorer;

import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.decorators.GradientEdgePaintTransformer;

public class EdgeRenderer extends GradientEdgePaintTransformer<TreeNode,Relation>{

	private boolean gradient = true;
	public void setGradient( boolean flag ) {
		gradient = flag;
	}
	
	private static EdgeColorer getEdgeColorer() {
		return Visualizer.getDataset().getEdgeColorer();
	}
	
	public EdgeRenderer(VisualizationViewer<TreeNode, Relation> vv) {
		super(getEdgeColorer().getBaseGradientColor(), getEdgeColorer().getBaseColor(), vv);
	}
	
	protected Color getColor1( Relation e ) {
		return getEdgeColorer().getColor1((Relation)e, vv.getPickedEdgeState().isPicked((Relation)e));
	}
	protected Color getColor2( Relation e ) {
		return getEdgeColorer().getColor2((Relation)e, vv.getPickedEdgeState().isPicked((Relation)e));
	}
	
	public Paint transform(Relation e) {
		if(gradient) {
			return super.transform(e);
		} else {
			return getColor2(e);
		}
	}
	
	@SuppressWarnings("rawtypes")
	public Transformer arrowFillPaintTransformer = new Transformer() {
		public Color transform(Object e) {
			if( AnalysisColorer.state() ) {
				return AnalysisColorer.getEdgeColor((Relation)e);
			}
			return getColor2((Relation)e);
		}
	};
	@SuppressWarnings("rawtypes")
	public Transformer arrowDrawPaintTransformer = new Transformer() {
		public Color transform(Object e) {
			if( AnalysisColorer.state() ) {
				return AnalysisColorer.getEdgeColor((Relation)e);
			}
			return getColor2((Relation)e);
		}
	};
	
	
	
}
