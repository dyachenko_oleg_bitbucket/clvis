package app.visualizer.renderers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

import javax.swing.JComponent;

import app.graph.Relation;
import app.graph.TreeNode;
import app.visualizer.Visualizer;
import app.visualizer.colorers.VertexColorer;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.Context;
import edu.uci.ics.jung.visualization.Layer;
import edu.uci.ics.jung.visualization.RenderContext;
import edu.uci.ics.jung.visualization.picking.PickedState;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import edu.uci.ics.jung.visualization.transform.shape.GraphicsDecorator;

public class VertexRenderer implements Renderer.Vertex<TreeNode,Relation> {

	PickedState<TreeNode> pickedState;
	boolean cyclic;
	
	private VertexColorer getVertexColorer() {
		return Visualizer.getDataset().getVertexColorer();
	}
	
	public VertexRenderer(PickedState<TreeNode> pickedState, boolean cyclic) {
		this.pickedState = pickedState;
		this.cyclic = cyclic;
	}
	
	@Override
	public void paintVertex(RenderContext<TreeNode, Relation> rc, Layout<TreeNode, Relation> layout, TreeNode v) {
		Graph<TreeNode,Relation> graph = layout.getGraph();
        if (rc.getVertexIncludePredicate().evaluate(Context.<Graph<TreeNode,Relation>,TreeNode>getInstance(graph,v))) {
            boolean vertexHit = true;
            
            // get the shape to be rendered
            Shape shape = rc.getVertexShapeTransformer().transform(v);
            
            Point2D p = layout.transform(v);
            p = rc.getMultiLayerTransformer().transform(Layer.LAYOUT, p);

            float x = (float)p.getX();
            float y = (float)p.getY();
            
            // create a transform that translates to the location of
            // the vertex to be rendered
            AffineTransform xform = AffineTransform.getTranslateInstance(x,y);
            // transform the vertex shape with xtransform
            shape = xform.createTransformedShape(shape);
            
            vertexHit = vertexHit(rc, shape);
                //rc.getViewTransformer().transform(shape).intersects(deviceRectangle);

            if (vertexHit) {
            	paintShapeForVertex(rc, v, shape);
            }
        }
		
	}
	
	
	protected boolean vertexHit(RenderContext<TreeNode,Relation> rc, Shape s) {
        JComponent vv = rc.getScreenDevice();
        Rectangle deviceRectangle = null;
        
        if(vv != null) {
            Dimension d = vv.getSize();
            deviceRectangle = new Rectangle(
            	0,0,
            	d.width,d.height);
        }
        return rc.getMultiLayerTransformer().getTransformer(Layer.VIEW).transform(s).intersects(deviceRectangle);
    }
	protected void paintShapeForVertex(RenderContext<TreeNode,Relation> rc, TreeNode v, Shape shape) {
        GraphicsDecorator g = rc.getGraphicsContext();
        Paint oldPaint = g.getPaint();
        Rectangle r = shape.getBounds();
        float y2 = (float)r.getMaxY();
        if(cyclic) {
        	y2 = (float)(r.getMinY()+r.getHeight()/2);
        }
        
        Paint fillPaint = null;
        boolean picked = pickedState != null && pickedState.isPicked(v);
        Color c1 = getVertexColorer().getColor1(v, picked);
        Color c2 = getVertexColorer().getColor2(v, picked);
        fillPaint = new GradientPaint(
        	(float)r.getMinX(), (float)r.getMinY(), c1, 
        	(float)r.getMinX(), y2, c2, 
        	cyclic
        );
        
        if(fillPaint != null) {
            g.setPaint(fillPaint);
            g.fill(shape);
            g.setPaint(oldPaint);
        }
        Paint drawPaint = rc.getVertexDrawPaintTransformer().transform(v);
        if(drawPaint != null) {
            g.setPaint(drawPaint);
        }
        Stroke oldStroke = g.getStroke();
        Stroke stroke = rc.getVertexStrokeTransformer().transform(v);
        if(stroke != null) {
            g.setStroke(stroke);
        }
        g.draw(shape);
        g.setPaint(oldPaint);
        g.setStroke(oldStroke);
    }

}
