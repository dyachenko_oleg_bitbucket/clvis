package app.visualizer.colorers;


import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.table.TableColumn;

import app.algorithm.LevelSizeLayout;
import app.data.Dataset;
import app.graph.TreeNode;
import app.gui.AbstractModel;
import app.gui.ColorColumnRenderer;
import app.gui.Table;
import app.visualizer.Visualizer;

public class VertexColorer {
	private Color baseColor = Color.BLUE;
	private Color baseGradientColor = Color.WHITE;
	private Color pickedColor = Color.RED;
	private Color pickedGradientColor = Color.WHITE;
	
	private Map<String,Color> ent_colors = new HashMap<String, Color>();
	private Map<String,Color> obj_colors = new HashMap<String, Color>();
	
	public void setEntityColor( String id, Color color ) {
		ent_colors.put(id, color);
	}
	public void removeEntityColor( String id ) {
		ent_colors.remove(id);
	}
	public void setObjectColor( String id, Color color ) {
		obj_colors.put(id, color);
	}
	public void removeObjectColor( String id ) {
		obj_colors.remove(id);
	}
	
	public void setBaseColor( Color color ) {
		baseColor = color;
	}
	public void setBaseGradientColor( Color color ) {
		baseGradientColor = color;
	}
	public void setPickedColor( Color color ) {
		pickedColor = color;
	}
	public void setPickedGradientColor( Color color ) {
		pickedGradientColor = color;
	}	
	
	/* get main color of vertex */
	public Color getBaseColor( TreeNode root ) {
		if( LevelSizeLayout.colors.containsKey(root.getId()) ) {
			return LevelSizeLayout.colors.get(root.getId());
		} else if( obj_colors.containsKey(root.getId()) ) {
			return obj_colors.get(root.getId()); 
		} else if( ent_colors.containsKey(root.getEntity()) ) {
			return ent_colors.get(root.getEntity());
		} else {
			return baseColor;
		}
	}
	public Color getBaseColor() {
		return baseColor;
	}
	
	/* secondary gradient color for vertex */
	public Color getBaseGradientColor() {
		return baseGradientColor;
	}
	public Color getPickedColor() {
		return pickedColor;
	}
	public Color getPickedGradientColor() {
		return pickedGradientColor;
	}
	
	public Color getColor1( TreeNode v, boolean picked ) {
		if( AnalysisColorer.state() ) {
			return AnalysisColorer.getVertexColor(v);
		}
		return picked ? getPickedColor() : getBaseColor(v);
	}
	public Color getColor2( TreeNode v, boolean picked ) {
		if( AnalysisColorer.state() ) {
			return AnalysisColorer.getVertexColor(v);
		}
		return picked ? getPickedGradientColor() : getBaseGradientColor();
	}
	
	public void getColorChooser() throws Exception {
		final JDialog dialog = new JDialog();
		dialog.setTitle("Задайте цвета вершинам");
		Container cont = dialog.getContentPane();
		cont.setLayout(new BoxLayout(cont, BoxLayout.PAGE_AXIS));
		
		Dimension dim = new Dimension(300,20);
		
		JButton baseColor = new JButton("Основной цвет");
		JButton baseGradientColor = new JButton("Вторичный цвет");
		JButton pickedColor = new JButton("Основной цвет (выделение)");
		JButton pickedGradientColor = new JButton("Вторичный цвет (выделение)");
		
		baseColor.setMinimumSize(dim);
		baseColor.setPreferredSize(dim);
		baseGradientColor.setMinimumSize(dim);
		baseGradientColor.setPreferredSize(dim);
				
		pickedColor.setMinimumSize(dim);
		pickedColor.setPreferredSize(dim);
		pickedGradientColor.setMinimumSize(dim);
		pickedGradientColor.setPreferredSize(dim);
		
		Box box = Box.createHorizontalBox();
		box.add(baseColor);
		box.add(Box.createHorizontalGlue());
		box.add(baseGradientColor);
		box.setMinimumSize(new Dimension(600,20));
		cont.add(box);
		
		box = Box.createHorizontalBox();
		box.add(pickedColor);
		box.add(Box.createHorizontalGlue());
		box.add(pickedGradientColor);
		box.setMinimumSize(new Dimension(600,20));
		cont.add(box);
		
		baseColor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Color color = JColorChooser.showDialog( dialog, "Выберите цвет", getBaseColor()	); 
				if( color != null ) {
					setBaseColor(color);
					Visualizer.vv.repaint();
				}
			}
		});
		baseGradientColor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Color color = JColorChooser.showDialog( dialog, "Выберите цвет", getBaseGradientColor()	); 
				if( color != null ) {
					setBaseGradientColor(color);
					Visualizer.vv.repaint();
				}
			}
		});
		pickedColor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Color color = JColorChooser.showDialog( dialog, "Выберите цвет", getPickedColor()	); 
				if( color != null ) {
					setPickedColor(color);
					Visualizer.vv.repaint();
				}
			}
		});
		pickedGradientColor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Color color = JColorChooser.showDialog( dialog, "Выберите цвет", getPickedGradientColor()	); 
				if( color != null ) {
					setPickedGradientColor(color);
					Visualizer.vv.repaint();
				}
			}
		});
		
		box = Box.createHorizontalBox();
		final Table table = new Table(new AbstractModel(
			new Vector<String>(Arrays.asList(new String[]{
				"ID сущности", "Имя сущности", "Цвет" 	
			})),
			new Vector<Class>(Arrays.asList(new Class[]{
				String.class,
				String.class,
				Color.class
			}))
		));
		
		ArrayList<String[]> ids = Visualizer.getDataset().getOrderedList( Dataset.ENTITY );
		for( String[] s : ids ) {
			if(s[1].equals("")) continue;
			Vector<Object> v = new Vector<Object>();
			v.add(s[0]);
			v.add(s[1]);
			v.add(ent_colors.containsKey(s[0]) ? ent_colors.get(s[0]) : getBaseColor());
			table.addRow(v);
		}
		
		table.addRenderer(1);
		TableColumn tm = table.getColumnModel().getColumn(2);
		tm.setCellRenderer(new ColorColumnRenderer());
		JScrollPane jsp = new JScrollPane(table);
		
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				Point pt = table.getSelectedCell();
				if ( e.getClickCount() == 1 && pt.y == 2 ) {
					if( pt != null ) {
						Color color = JColorChooser.showDialog(
								table, "Выберите цвет", (Color)table.getSelectedValue());
						if( color != null ) {
							setEntityColor((String)table.getValueAt(pt.x, 0), color);
							table.setValueAt(color, pt.x, pt.y);
							table.repaint();
							Visualizer.vv.repaint();
						}
					}
				}
			}
		});
		box.add(jsp);
		jsp.setMinimumSize(new Dimension(600,200));
		jsp.setPreferredSize(new Dimension(600,200));
		if( table.getRowCount() != 0 ) {
			cont.add(box);
		}
		
		box = Box.createHorizontalBox();
		final Table table1 = new Table(new AbstractModel(
			new Vector<String>(Arrays.asList(new String[]{
				"ID объекта", "Имя объекта", "Цвет" 	
			})),
			new Vector<Class>(Arrays.asList(new Class[]{
				String.class,
				String.class,
				Color.class
			}))
		));
		
		//***** new one 
		ids = Visualizer.getDataset().getOrderedList( Dataset.OBJECT );
		for( String[] s : ids ) {
			if(s[1].equals("")) continue;
			Vector<Object> v = new Vector<Object>();
			v.add(s[0]);
			v.add(s[1]);
			v.add(obj_colors.containsKey(s[0]) ? obj_colors.get(s[0]) : getBaseColor());
			table1.addRow(v);
		}
		
		table1.addRenderer(1);
		tm = table1.getColumnModel().getColumn(2);
		tm.setCellRenderer(new ColorColumnRenderer());
		jsp = new JScrollPane(table1);
		
		table1.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				Point pt = table1.getSelectedCell();
				if ( e.getClickCount() == 1 && pt.y == 2 ) {
					if( pt != null ) {
						Color color = JColorChooser.showDialog(
								table1, "Выберите цвет", (Color)table1.getSelectedValue());
						if( color != null ) {
							setObjectColor((String)table1.getValueAt(pt.x, 0), color);
							table1.setValueAt(color, pt.x, pt.y);
							table1.repaint();
							Visualizer.vv.repaint();
						}
					}
				}
			}
		});
		box.add(jsp);
		jsp.setMinimumSize(new Dimension(600,200));
		jsp.setPreferredSize(new Dimension(600,200));
		if( table1.getRowCount() != 0 ) {
			cont.add(box);
		}
		
		
		dialog.setSize( 600,450 );
		dialog.setModal(true);
		dialog.setVisible(true);
	}
	
	
	
	
	
	
	
	
	
}
