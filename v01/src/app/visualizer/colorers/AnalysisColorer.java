package app.visualizer.colorers;


import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.table.TableColumn;

import app.graph.ArrayListClone;
import app.graph.Relation;
import app.graph.TreeEdge;
import app.graph.TreeNode;
import app.gui.AbstractModel;
import app.gui.Table;
import app.visualizer.Visualizer;


public class AnalysisColorer {
	private static boolean flag = false;
	public static void toggle() {
		flag = !flag;
	}
	public static void on() {
		flag = true;
	}
	public static void off() {
		flag = false;
	}
	public static boolean state() {
		return flag;
	}
	
	public static Color getEdgeColor(Relation e) {
		if( AnalysisColorer.edge.contains(e.getId()) ) {
			return Color.RED; 
		}
		return Color.LIGHT_GRAY;
	}
	public static Color getVertexColor(TreeNode v) {
		if( AnalysisColorer.vertex.contains(v.getId()) ) {
			return Color.RED; 
		}
		return Color.LIGHT_GRAY;
	}
	
	
	public static ArrayList<String> vertex = new ArrayList<String>();
	public static ArrayList<String> edge = new ArrayList<String>();
	
	public static class AnalysisResult {
		public ArrayList<String> vertex = new ArrayList<String>();
		public ArrayList<String> edge = new ArrayList<String>();
		public String note = "Text";
		public AnalysisResult() {
			// 
		}
	} 
	
	private static void showResults() {
		AnalysisResult ars = results.get(currentResult);
		vertex = ars.vertex;
		edge = ars.edge;
		Visualizer.setCenter( Visualizer.getDataset().getTreeNodes().get(vertex.get(0)) );
		Visualizer.vv.repaint();
	}
	private static ArrayList<AnalysisResult> getResultsByCode(int code) {
		ArrayList<AnalysisResult> ars = new ArrayList<AnalysisColorer.AnalysisResult>();
		switch( code ) {
		case 1: // поиск циклов
			ars.addAll( getCycles() );
		break;
		}
		return ars;
	}
	
	
	public static final ArrayList<AnalysisResult> results = new ArrayList<AnalysisResult>();
	public static int currentResult = 0;
	
	
	public static void getChooser() throws Exception {
		final JDialog dialog = new JDialog();
		dialog.setTitle("Выберите свойства для анализа");
		Container cont = dialog.getContentPane();
		cont.setLayout(new BoxLayout(cont, BoxLayout.PAGE_AXIS));
		
		JButton clean = new JButton("Очистить");
		JButton run = new JButton("Запустить");
		JButton next = new JButton("Следующее");
		JButton prev = new JButton("Предыдущее");
		
		Dimension dim = new Dimension(300,20);
		
		clean.setMinimumSize(dim);
		clean.setPreferredSize(dim);
		run.setMinimumSize(dim);
		run.setPreferredSize(dim);
		next.setMinimumSize(dim);
		next.setPreferredSize(dim);
		prev.setMinimumSize(dim);
		prev.setPreferredSize(dim);
		
		Box box = Box.createHorizontalBox();
		box.add(clean);
		box.add(Box.createHorizontalGlue());
		box.add(run);
		box.setMinimumSize(new Dimension(600,20));
		cont.add(box);
		
		box = Box.createHorizontalBox();
		box.add(prev);
		box.add(Box.createHorizontalGlue());
		box.add(next);
		box.setMinimumSize(new Dimension(600,20));
		cont.add(box);
		
		box = Box.createHorizontalBox();
		final Table table = new Table(new AbstractModel(
			new Vector<String>(Arrays.asList(new String[]{
				"#","Свойство", "+" 	
			})),
			new Vector<Class>(Arrays.asList(new Class[]{
				Integer.class,
				String.class,
				Boolean.class
			}))
		));
		
		Vector<Object> v = new Vector<Object>();
		v.add(1);
		v.add("Поиск циклов");
		v.add(true);
		table.addRow(v);
		
		table.addRenderer(1);
		TableColumn tm = table.getColumnModel().getColumn(1);
		table.setColWidth(0, 30);
		table.setColWidth(2, 30);
		JScrollPane jsp = new JScrollPane(table);
		box.add(jsp);
		jsp.setMinimumSize(new Dimension(580,200));
		jsp.setPreferredSize(new Dimension(600,200));
		cont.add(box);
		
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				Point pt = table.getSelectedCell();
				if ( e.getClickCount() == 1 && pt.y == 2 ) {
					if( pt != null ) {
						table.setValueAt(!(Boolean)table.getValueAt(pt.x, pt.y), pt.x, pt.y);
						table.repaint();
					}
				}
			}
		});
		
		clean.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				AnalysisColorer.off();
				results.clear();
				for( int i=0; i<table.getRowCount(); i++ ) {
					table.setValueAt(false, i, 2);
				}
				table.repaint();
				Visualizer.vv.repaint();
			}
		});
		run.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				AnalysisColorer.on();
				for( int i=0; i<table.getRowCount(); i++ ) {
					if( (Boolean)table.getValueAt(i, 2) ) {
						results.addAll(getResultsByCode((Integer)table.getValueAt(i, 0)));
					}
				}
			}
		});
		next.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if( currentResult + 1 < results.size() ) {
					currentResult++;
				}
				if( currentResult < results.size() ) {
					showResults();
				}
			}
		});
		prev.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if( currentResult - 1 >= 0 ) {
					currentResult--;
				}
				if( currentResult < results.size() ) {
					showResults();
				}
			}
		});
		
		results.clear();
		
		dialog.setSize( 600,450 );
		//dialog.setModal(true);
		dialog.setVisible(true);
		dialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				AnalysisColorer.off();
				results.clear();
				for( int i=0; i<table.getRowCount(); i++ ) {
					table.setValueAt(false, i, 2);
				}
				table.repaint();
				Visualizer.vv.repaint();
			}
		});
	}
	
	
	private static ArrayList<AnalysisResult> getCycles() {
		ArrayList<AnalysisResult> out = new ArrayList<AnalysisResult>();
		for( TreeNode root : Visualizer.currentList ) {
			out.addAll( getCycles(root) );
		}
		return out;
	}
	private static ArrayList<AnalysisResult> getCycles(TreeNode root) {
		ArrayList<AnalysisResult> out = new ArrayList<AnalysisResult>();
		ArrayList<TreeNode> nodes = root.getAllNodes();
		ArrayList<String> cycles = new ArrayList<String>();
		while( !nodes.isEmpty() ) {
			cycles.addAll( getCycles(nodes) );
			nodes.remove(0);
		}
		for( String s: cycles ) {
			AnalysisResult ars = new AnalysisResult();  
			String[] split = s.split("\\|");
			String[] path = split[0].split(" ");
			for( String s_: path ) {
				ars.vertex.add(s_);
			}
			path = split[1].split(" ");
			for( String s_: path ) {
				ars.edge.add(s_);
			}
			ars.note = "Цикл";
			out.add(ars);
		}

		return out;
	}
	
	
	private static ArrayListClone<String> cloner1 = new ArrayListClone<String>();
	private static ArrayListClone<TreeNode> cloner2 = new ArrayListClone<TreeNode>();
	
	private static ArrayList<String> getCycles(ArrayList<TreeNode> nodes) {
		//ArrayList<String> out = new ArrayList<String>();
		
		ArrayList<String> node_path = new ArrayList<String>();
		ArrayList<String> edge_path = new ArrayList<String>();
		ArrayList<TreeNode> nodes_clone = cloner2.get(nodes);
		nodes_clone.remove(0);
		
		return getCycles(nodes.get(0), nodes_clone, node_path, edge_path);
		//return out;
	}
	private static ArrayList<String> getCycles(TreeNode root, ArrayList<TreeNode> nodes, ArrayList<String> node_path, ArrayList<String> edge_path) {
		ArrayList<String> out = new ArrayList<String>();
		node_path.add(root.getId());
		for( TreeEdge te: root.getEdges() ) {
			if( !te.getRelationPair().getDirection() ) continue;
			if( te.getTreeNode().getId().equals(node_path.get(0)) ) {
				// cycle
				if( node_path.size() > 2 ) {
					String node_string = node_path.get(0);
					for( int i=1; i<node_path.size(); i++ ) {
						node_string += " " + node_path.get(i);
					} 
					String edge_string = edge_path.get(0);
					for( int i=1; i<edge_path.size(); i++ ) {
						edge_string += " " + edge_path.get(i);
					}
					edge_string += " " + te.getRelationPair().getRelation().getId();
					out.add( node_string + "|" + edge_string);
				}
			} else if( nodes.contains(te.getTreeNode()) ) {
				// clone arraylists
				ArrayList<TreeNode> nodes_clone = cloner2.get(nodes);
				nodes_clone.remove(te.getTreeNode());
				ArrayList<String> node_path_clone = cloner1.get(node_path);
				//node_path_clone.add(te.getTreeNode().getId());
				ArrayList<String> edge_path_clone = cloner1.get(edge_path);
				edge_path_clone.add(te.getRelationPair().getRelation().getId());
				out.addAll( getCycles(te.getTreeNode(), nodes_clone, node_path_clone, edge_path_clone) );
			}
		}
		return out;
	}
}
