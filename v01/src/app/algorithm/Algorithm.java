package app.algorithm;


import java.awt.Dimension;
import java.util.ArrayList;

import app.App;
import app.graph.Point;
import app.graph.TreeNode;
import app.visualizer.layout.VisualizerPixelTransformer;


public abstract class Algorithm implements AlgorithmInterface{
	@Override
	public void setCoordinates(TreeNode root) {
		/*
		 * The main method:
		 * set coordinates for the connected component 
		 * */
	}
	@Override
	public void setCoordinates(ArrayList<TreeNode> list) {
		/*
		 * The main method: 
		 * set coordinates for all connected components
		 * */
	}	

	public static void move( TreeNode root, Point pt ) {
		root.pos.move(pt);
		ArrayList<TreeNode> children = root.getChildren();  
		if( children.size() == 0 ) return;
		for( TreeNode tn : root.getChildren() ) {
			move(tn, pt);
		}	
	}
	
	public Point max( TreeNode root ) {
		Point pt = root.pos; 
		for( TreeNode tn : root.getChildren() ) {
			pt = Point.max(pt, max(tn));
		}
		return pt;
	}
	public Point min( TreeNode root ) {
		Point pt = root.pos; 
		for( TreeNode tn : root.getChildren() ) {
			pt = Point.min(pt, min(tn));
		}
		return pt;
	}
	public Point size( TreeNode root ) {
		Point min = min(root);
		return max(root).movex(-min.x).movey(-min.y).move(new Point(1,1));
	}
	
	public static int code = 0;
	
	public final static int ALG_LEVEL = 0;
	public final static int ALG_RADIAL = 1;
	public final static int ALG_CIRCLE = 2;
	public final static int ALG_BALLOON = 3;
	public final static int ALG_SPRING = 4; // Eades
	public final static int ALG_FRRE = 5; // Fr&Re
	public final static int ALG_FRICK = 6; // Frick
	public final static int ALG_KK = 7; // KamadaKawai
	
	public static Algorithm getInstance( int code ) {
		switch( code ) {
		case ALG_LEVEL : return new LevelLayout();
		case ALG_RADIAL : 
			//return new LevelSizeLayout();
			return new RadialLayout();
		case ALG_CIRCLE : return new CircleLayout();
		case ALG_BALLOON : return new BalloonLayout();
		case ALG_SPRING : return new SpringLayout();
		case ALG_FRRE : return new FruchtermanReingoldLayout();
		case ALG_FRICK : return new FrickLayout();
		case ALG_KK : return new KamadaKawaiLayout();
		default: return new LevelLayout();
		}
	}
	
	
	protected void placeComponents(ArrayList<TreeNode> list) {
		if( list.size() == 0 ) return;
		
		ArrayList<Point> min = new ArrayList<Point>();
		ArrayList<Point> max = new ArrayList<Point>();
		
		/*
		for( int i=0; i<list.size(); i++ ) {
			min.add( min(list.get(i)) );
			max.add( max(list.get(i)) );
		}
		double height = max.get(0).y - min.get(0).y;
		Point corner = new Point(0,0);
		double right = 0;
		for( int i=0; i<list.size(); i++ ) {
			Point size = max.get(i).clone().move( min.get(i).clone().minus() );
			if( corner.y + size.y > 1.2*height ) {
				// new column
				
			} else {
				// place to the corner
				
			}
		}
		*/
		//don't remove - it's simple placement 
		move(list.get(0), min(list.get(0)).minus() );
		double y_level = list.get(0).pos.y;
		for( int i=1; i<list.size(); i++ ) {
			Point max_ = max(list.get(i-1));
			Point min_ = min(list.get(i));
			double x = max_.x - min_.x + 2;
			double y = y_level - list.get(i).pos.y;
			move(list.get(i), new Point(x,y) );
		}
		
		
		Point size = new Point(0,0);
		for( int i=0; i<list.size(); i++ ) {
			size = Point.max(size,max(list.get(i)));
			break;
		}
		size.movex(2).movey(2);
		
		Dimension dim = App.getDimension();
		
		
		double maxx = (double)(dim.getWidth()/VisualizerPixelTransformer.resize);
		double maxy = (double)(dim.getHeight()/VisualizerPixelTransformer.resize);
		
		
		double scale = size.x*maxy > size.y*maxx ? size.x/maxx : size.y/maxy;   
		for( int i=0; i<list.size(); i++ ) {
			scale(list.get(i), scale);
		}
	}
	protected static void scale( TreeNode root, double mult ) {
		root.pos.mult(1/mult);
		for(TreeNode tn : root.getChildren()) {
			scale(tn,mult);
		}
	}
	protected static void scalex( TreeNode root, double mult ) {
		root.pos.setx(root.pos.x/mult);
		for(TreeNode tn : root.getChildren()) {
			scalex(tn,mult);
		}
	}
	protected static void scaley( TreeNode root, double mult ) {
		root.pos.sety(root.pos.y/mult);
		for(TreeNode tn : root.getChildren()) {
			scaley(tn,mult);
		}
	}
}
