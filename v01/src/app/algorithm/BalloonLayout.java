package app.algorithm;


import java.util.ArrayList;
import java.util.HashMap;

import app.graph.Point;
import app.graph.Point3;
import app.graph.TreeNode;

public class BalloonLayout extends Algorithm {
	
	// alpha innerRadius outerRadius 
	HashMap<TreeNode, Point3> data = new HashMap<TreeNode, Point3>();
	double node = 0.1;
	double dist = 0.1;
	
	@Override
	public void setCoordinates(TreeNode root) {
		root.setLevels();
		initDataset(root);
		setCoordinatesStep(root);
		setAbsPositions(root);
	}
	@Override
	public void setCoordinates(ArrayList<TreeNode> list) {
		for( TreeNode root : list ) {
			setCoordinates(root);
		}
		// TODO Think of cool layout for CCs
		placeComponents(list);
		/*
		for( int i=1; i<list.size(); i++ ) {
			move(list.get(i), new Point( max(list.get(i-1)).x + list.get(i).pos.x - min(list.get(i)).x + 2 , 0));
		}
		*/
	}	
	
	private void setCoordinatesStep( TreeNode root ) {
		ArrayList<TreeNode> childs = root.getChildren();
		if( childs.isEmpty() ) {
			data.get(root).setx(0).sety(node).setz(0);
		} else {
			for( TreeNode tn : childs ) {
				setCoordinatesStep(tn);
			}
			double maxR = 0;
			for( TreeNode tn : childs ) {
				maxR = data.get(tn).y > maxR ? data.get(tn).y : maxR;		
			}
			// resize 
			for( TreeNode tn : childs ) {
				multRadius(tn, maxR/data.get(tn).y);
			}
			// radius
			double R = 0;
			if( childs.size() == 1 ) {
				R = 1.2*maxR;
				data.get(childs.get(0)).setx(Math.PI).setz(R);
			} else if( childs.size() == 2 ) {
				R = 1.2*maxR;
				data.get(childs.get(0)).setx(-0.5*Math.PI).setz(R);
				data.get(childs.get(1)).setx(0.5*Math.PI).setz(R);
			} else { 
				double alpha = Math.PI / childs.size();
				R = maxR / Math.sin(alpha);
				for( int i=0; i<childs.size(); i++ ) {
					data.get(childs.get(i))
						.setx((2*i+1)*alpha)
					//	.sety( data.get(root).y * Math.sin(alpha) / (1+Math.sin(alpha)))
						.setz( R );
				}
			}
			data.get(root).sety(R+maxR);
		}
	}
	
	
	// init
	private void initDataset( TreeNode root ) {
		data.put(root, new Point3(0,0,0));
		for( TreeNode tn : root.getChildren() ) {
			initDataset(tn);
		}
	}
	private void setAbsPositions( TreeNode root ) {
		root.pos.set( new Point(0,0) );
		setAbsPositionsStep(root, 0);
	}
	
	private void setAbsPositionsStep(TreeNode root, double incAngle) {
		for( TreeNode tn : root.getChildren() ) {
			tn.pos
				.setx( root.pos.x + data.get(tn).z * Math.cos(incAngle + data.get(tn).x  - Math.PI ) )
				.sety( root.pos.y + data.get(tn).z * Math.sin(incAngle + data.get(tn).x  - Math.PI ) );
			setAbsPositionsStep(tn, incAngle + data.get(tn).x - Math.PI);
		}
	}
	private void multRadius( TreeNode root, double mult ) {
		data.get(root)
			.sety( data.get(root).y * mult )
			.sety( data.get(root).z * mult );
		for( TreeNode tn : root.getChildren() ) {
			multRadius(tn, mult);
		}
	}
	
	
}
