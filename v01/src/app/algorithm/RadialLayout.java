package app.algorithm;


import java.util.ArrayList;
import java.util.HashMap;

import app.graph.Point;
import app.graph.TreeNode;

public class RadialLayout extends Algorithm {
	
	// start_angle core_angle 
	HashMap<TreeNode, Point> data = new HashMap<TreeNode, Point>();
	
	boolean common = false;
	
	
	@Override
	public void setCoordinates(TreeNode root) {
		root.setLevels();
		setCoordinatesStep(root);
	}
	@Override
	public void setCoordinates(ArrayList<TreeNode> list) {
		if( common ) {
			int leaves = 0;
			for( TreeNode root : list ) {
				leaves += root.countLeaves(); 
			}
			double angle = 0;
			double angle_plus = 0;
			for( int i=0; i<list.size(); i++ ) {
				angle_plus = 2*Math.PI*list.get(i).countLeaves()/leaves;
				data.put(list.get(i), new Point(angle,angle_plus));
				double tn = list.get(i).getLevel() == 1 ? 0.5 : list.get(i).getLevel();
				list.get(i).pos = new Point( 
					tn * Math.cos(data.get(list.get(i)).x + 0.5*data.get(list.get(i)).y),
					tn * Math.sin(data.get(list.get(i)).x + 0.5*data.get(list.get(i)).y)
				);
				angle += angle_plus;
			}
		} else {
			for( TreeNode root : list ) {
				data.put(root, new Point(0,2*Math.PI));
				root.pos = new Point(0,0);
			}
		}
		for( TreeNode root : list ) {
			setCoordinates(root);
		}
		// TODO Think of cool layout for CCs
		//for( int i=1; i<list.size(); i++ ) {
		//	if(!common) move(list.get(i), new Point( max(list.get(i-1)).x + list.get(i).pos.x - min(list.get(i)).x + 2 , 0));
		//}
		if( !common ) {
			placeComponents(list);
		}
	}	
	
	private void setCoordinatesStep( TreeNode root ) {
		double delta = 0;
		for( TreeNode tn : root.getChildren() ) {
			data.put( tn, 
				new Point(data.get(root).x+delta, data.get(root).y * tn.countLeaves() / root.countLeaves() )
			);
			delta += data.get(tn).y;
			double angle = 
				data.get(tn).x + 
				data.get(tn).y * 0.5; 
			tn.pos = new Point( 
				(tn.getLevel()-(common?0:1)) * Math.cos(angle),
				(tn.getLevel()-(common?0:1)) * Math.sin(angle)
			);
			setCoordinatesStep(tn);
		}
	}
	
	
	
	
}
