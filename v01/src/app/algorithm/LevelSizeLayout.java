package app.algorithm;


import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.jdom2.Element;

import app.App;
import app.graph.Point;
import app.graph.TreeNode;
import app.visualizer.Visualizer;
import app.visualizer.layout.VisualizerPixelTransformer;


public class LevelSizeLayout extends Algorithm {
	
	public static boolean on = false;
	public static HashMap<String, Integer> sizes = new HashMap<String, Integer>();
	public static HashMap<String, Double> sizes2 = new HashMap<String, Double>();
	
	// base size
	public static HashMap<String, Point> data = new HashMap<String, Point>();
	public static HashMap<String, Color> colors = new HashMap<String, Color>();
	
	
		
	@Override
	public void setCoordinates(ArrayList<TreeNode> list) {
		initSizes();
		for( TreeNode root : list ) {
			initSizesCumulative(root);
		}
		setOrder(list);
		for( TreeNode root : list ) {
			setCoordinates(root);
		}
		double count = 0;
		for( TreeNode root : list ) {
			setAbsPositions(root, count);
			count += sizes2.get(root.getId());
		}
		placeComponents2(list);
	}
	
	@Override
	public void setCoordinates(TreeNode root) {
		root.setLevels();
		changeCumulativeSizes(root);
		setCoordinatesStep(root, 0);
	}
	
	private void setOrder(ArrayList<TreeNode> list) {
		ArrayList<Integer> childs = new ArrayList<Integer>();
		for( TreeNode root : list ) {
			childs.add(root.getChildren().size());
		}
		int[] nums = new int[list.size()];
		for( int i=0; i<list.size(); i++ ) {
			nums[i] = i;
		}
		for( int i=0; i<list.size(); i++ ) {
			for( int j=0; j<i; j++ ) {
				if( (childs.get(i) < childs.get(j)) ||
					((childs.get(i) == childs.get(j)) && (sizes2.get(list.get(i).getId()) > sizes2.get(list.get(j).getId())))
				) {
					int swap = nums[i]; 
					nums[i] = nums[j];
					nums[j] = swap;
					
					swap = childs.get(i);
					childs.set(i, childs.get(j));
					childs.set(j, swap);
				}
			}
		}
		ArrayList<TreeNode> list2 = new ArrayList<TreeNode>();
		for( int i=0; i<nums.length; i++ ) {
			list2.add( list.get(nums[i]) );//System.out.println();
		}
		list.clear();
		/*
		int i=0, j=list2.size()-1;
		while( i<=j ) {
			list.add(list2.get(i));
			i++;
			if(i<j) {
				list.add(list2.get(j));
				j--;
			}
		}
		*/
		for(int i=list2.size()-1; i>=0; i--) {
			list.add(list2.get(i));
		}
	}
	private void initSizes() {
		ArrayList<Element> search = Visualizer.getDataset().getXMLf().search("objects","object");
		for( Element e : search ) {
			sizes.put( e.getAttributeValue("id"), Integer.parseInt(e.getAttributeValue("count")) );
		}
	}
	private void initSizesCumulative(TreeNode root) {
		int count = sizes.get(root.getId());
		ArrayList<TreeNode> childs = root.getChildren();
		for( TreeNode tn : childs ) {
			initSizesCumulative(tn);
			count += sizes2.get(tn.getId());
		}
		sizes2.put(root.getId(), (double)count);
	}
	private void changeCumulativeSizes(TreeNode root) {
		/*
		int count = sizes.get(root.getId());
		ArrayList<TreeNode> childs = root.getChildren();
		for( TreeNode tn : childs ) {
			initSizesCumulative(tn);
			count += sizes2.get(tn.getId());
		}
		sizes2.put(root.getId(), (double)count);
		*/
	}
	private void setCoordinatesStep(TreeNode root, double level) {
		Random rand = new Random();
		float r = rand.nextFloat();
		float g = rand.nextFloat();
		float b = rand.nextFloat();
		Color randomColor = new Color(r, g, b);
		
		colors.put(root.getId(), randomColor);
		
		data.put( root.getId(), new Point(level, sizes2.get(root.getId())) );
		ArrayList<TreeNode> childs = root.getChildren();
		int count = 0;
		for( TreeNode tn : childs ) {
			count += sizes2.get(tn.getId());
		}
		double lev = level + 0.5*( sizes2.get(root.getId()) - count );
		for( TreeNode tn : childs ) {
			setCoordinatesStep(tn, lev);
			lev += sizes2.get(tn.getId());
		}
	}
	private void setAbsPositions( TreeNode root, double count ) {
		Point pt = data.get(root.getId());
		root.pos.setx(root.getLevel()-1).sety(count + pt.x + 0.5*pt.y);
		//blocks.put(root.getId(), new Point(pt.x,pt.y));
		ArrayList<TreeNode> childs = root.getChildren();
		for( TreeNode tn : childs ) {
			setAbsPositions(tn, count);
		}
	}
	
	public static double scaley = 1;
	public static double scalex = 1;
	public static double width = 1;
	
	protected void placeComponents2(ArrayList<TreeNode> list) {
		if( list.size() == 0 ) return;
		
		// count of the elements
		double count = 0;
		for( TreeNode root : list ) {
			count += sizes2.get(root.getId());
			break;
		}
		
		int height = 1;
		for( TreeNode root : list ) {
			int h = root.getHeight(); 
			if( h > height ) {
				height = h;
			}
		}
		
		Dimension dim = App.getDimension();
		
		double maxx = (double)(dim.getWidth()/VisualizerPixelTransformer.resize);
		double maxy = (double)(dim.getHeight()/VisualizerPixelTransformer.resize);
				
		scaley = 1.3*count/maxy;
		scalex = height/maxx;
		width = 1/scalex;
		
		for( int i=0; i<list.size(); i++ ) {
			scalex(list.get(i), scalex);
			scaley(list.get(i), scaley);
		}
	}
	
	
}
