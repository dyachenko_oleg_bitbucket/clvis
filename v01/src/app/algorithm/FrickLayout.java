package app.algorithm;


import java.util.ArrayList;
import java.util.HashMap;

import app.App;
import app.graph.Point;
import app.graph.TreeEdge;
import app.graph.TreeNode;
import app.visualizer.layout.VisualizerPixelTransformer;


public class FrickLayout extends Algorithm {
	
	private int stepCount = 100; 
	public double delta( int step ) {
		return 0.1;
	}
	double l = 2;
	double cgrav = 1;	
	
	// force 
	HashMap<TreeNode, ArrayList<TreeNode>> nodes = new HashMap<TreeNode, ArrayList<TreeNode>>();
	//HashMap<TreeNode, ArrayList<Pair<TreeNode>>> edges = new HashMap<TreeNode, ArrayList<Pair<TreeNode>>>();
	
	private double fr( double x, double l ) {
		return l*l/(x*x);
	}
	private double fa( double x, double l, int degv ) {
		return x*x/(l*fv(degv));
	}
	private double fv(int degv) {
		return 1+0.5*degv;
	}
	
	
	private double temp(int step) {
		return 5/Math.log(step+1);
	}
	
	
	@Override
	public void setCoordinates(TreeNode root) {
		root.setLevels();
		if( !nodes.containsKey(root) ) {
			nodes.put(root,root.getAllNodes());
		}
		for( int i=1; i<=stepCount; i++ ) {
			setCoordinatesStep(root, i);
		}
	}
	@Override
	public void setCoordinates(ArrayList<TreeNode> list) {
		for( TreeNode root : list ) {
			setCoordinates(root);
		}
		// TODO Think of cool layout for CCs
		placeComponents(list);
	}	
	
	private void setCoordinatesStep( TreeNode root, int step ) {
		ArrayList<TreeNode> _nodes = nodes.get(root);
		//ArrayList<Pair<TreeNode>> _edges = edges.get(root);
		HashMap<TreeNode, Point> forces = new HashMap<TreeNode, Point>();
		
		for( TreeNode v: _nodes ) {
			forces.put(v, new Point(0,0));
			for( TreeNode u: _nodes ) {
				if( !u.equals(v) ) {
					Point force = v.pos.clone().move(u.pos.clone().minus());
					double dist = force.abs(); 
					force.mult(1/dist);
					forces.get(v).move( force.clone().mult(fr(dist,l)) );
				}
			}
			for( TreeEdge te : v.getEdges() ) {
				Point force = v.pos.clone().move(te.getTreeNode().pos.clone().minus());
				double dist = force.abs(); 
				force.mult(1/dist);
				forces.get(v).move( force.clone().mult(-fa(dist,l,v.getChildren().size())) );
			}
		}
		
		Point mass_center = new Point(0,0); 
		for( TreeNode v: _nodes ) {
			mass_center.move(v.pos);
		}
		mass_center.mult((double)1/_nodes.size());
		for( TreeNode v: _nodes ) {
			Point pt = v.pos.clone().move(mass_center.minus()).mult(cgrav*fv(v.getChildren().size()));
			forces.get(v).move( pt.minus() );
		}
		
		
		for( TreeNode v: _nodes ) {
			Point pt = forces.get(v).clone();
			double pt_dist = pt.abs();
			if( pt_dist > temp(step) ) {
				pt.mult(temp(step));
				pt.mult((double)1/pt_dist);
			}
			v.pos.move(pt);
		}
	}
}
