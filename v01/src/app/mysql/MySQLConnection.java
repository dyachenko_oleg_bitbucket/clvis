package app.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQLConnection {
	
	private Connection conn = null;
	
	public MySQLConnection(String dbhost, String dbname, String dbuser, String dbpass) throws Exception{
		// load driver
		try{
			Class.forName("com.mysql.jdbc.Driver");
		}
		catch(Exception e){
			throw new Exception("Unable to load mysql driver.\n");
		}
		// establish connection
		try{
			this.conn = DriverManager.getConnection("jdbc:mysql://"+dbhost+"/"+dbname+"?useUnicode=true&characterEncoding=UTF8", dbuser, dbpass);
		}
		catch(SQLException e){
			throw new Exception("Unable to connect to mysql database.\nResponse:\n"+e.getMessage());
		}		
	}
	
	public int sql(String query) throws SQLException {
		return conn.createStatement().executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
	}
	public int sql(Statement statement, String query) throws SQLException{
		return statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
	}
	public ResultSet query(String query) throws SQLException{
		return conn.createStatement().executeQuery(query);
	}
	public ResultSet query(Statement statement, String query) throws SQLException{
		return statement.executeQuery(query);
	}
	
	public void close() {
		try {
			conn.close();
		} catch (SQLException e) {}
	}
	
}
