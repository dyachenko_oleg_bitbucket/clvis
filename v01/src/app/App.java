package app;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import app.algorithm.Algorithm;
import app.config.Configuration;
import app.data.DatasetData;
import app.data.DatasetOnto;
import app.gui.MenuBar;
import app.visualizer.Visualizer;
import app.visualizer.colorers.AnalysisColorer;
import edu.uci.ics.jung.visualization.GraphZoomScrollPane;



public class App extends JApplet {
	private static final long serialVersionUID = 1L;

	public static int WIDTH = 1100;
	public static int HEIGHT = 600;
	
	public final JPanel side = new JPanel();
	public final JPanel cont = new JPanel();
	public GraphZoomScrollPane gzp;
	
	private Dimension buttonDim = new Dimension(48,48);
	// side buttons
	public JButton scale_inc = new JButton( new ImageIcon("+.png") );
	public JButton scale_dec = new JButton( new ImageIcon("-.png") );
	public JButton refresh = new JButton( new ImageIcon("refresh.png") );
	public JButton analysis = new JButton( "Anallyze" );
	
	//TreeNode tn0;
	
	public void init() {
		Container pane = getContentPane();	
		//pane.setBackground(Color.RED);
		
		
		Toolkit toolkit =  Toolkit.getDefaultToolkit ();
		Dimension dim = toolkit.getScreenSize();

		WIDTH = (int)Math.round(dim.width*0.8);
		HEIGHT = (int)Math.round(dim.height*0.8);

		try{
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel");
			SwingUtilities.updateComponentTreeUI(this);
		} catch(Exception e){};

				
		setJMenuBar(new MenuBar());
		setSize(WIDTH,HEIGHT);
		
		side.setPreferredSize(new Dimension(60,HEIGHT));
		side.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 3, Color.GRAY));
		
		pane.add(side,BorderLayout.LINE_START);
		pane.add(cont,BorderLayout.CENTER);
		
		scale_inc.setPreferredSize(buttonDim);
		scale_dec.setPreferredSize(buttonDim);
		refresh.setPreferredSize(buttonDim);
		analysis.setPreferredSize(buttonDim);
		side.add(scale_dec);
		side.add(scale_inc);
		side.add(refresh);
		side.add(analysis);
		
		
		
		JButton run = new JButton("!!!");
		run.setPreferredSize(buttonDim);
		//side.add(run);
		run.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO
				Visualizer.repaintVisualization();
			}
		});
		
		
		Configuration.init();
		
		DatasetData dtd = new DatasetData();
		dtd.setXML("../"+Configuration.get("dataPath"));
		dtd.init();
		dtd.initFilters();
		dtd.applyFilters(Configuration.get("dataFilterPath"));
		dtd.setFocus("40");
		DatasetOnto dto = new DatasetOnto();
		dto.setXML("../"+Configuration.get("ontoPath"));
		dto.init();
		dto.initFilters();
		dto.applyFilters(Configuration.get("ontoFilterPath"));
		dto.setFocus("1");
		
		Visualizer.datasets.add(dtd);
		Visualizer.datasets.add(dto);
		
		//tn0 = dto.getConnectedComponents("1").get(0);
		//tn0 = dtd.getConnectedComponents("40").get(0);
		
		
		Visualizer.init();
		
		//tn0.setLevels();
		//System.out.println(tn0.countElements());		
		
		//Visualizer.changeVisualization( false, new RadialLayout(), "40" );
		//Visualizer.changeVisualization( false, new LevelLayout(), "40" );
		
		
		
	
		
        // create graph pabel 
		gzp = new GraphZoomScrollPane(Visualizer.vv);
		final GraphZoomScrollPane panel = gzp;
        panel.setPreferredSize(getDimension());
        cont.add(panel);
        
        
        //AnalysisColorer.vertex.add("6");
        //AnalysisColorer.edge.add("6>539");
        //AnalysisColorer.on();
        
        // scaling
        scale_inc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	Visualizer.scaler.scale(Visualizer.vv, 1.1f, Visualizer.vv.getCenter());
            }
        });
        scale_dec.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	Visualizer.scaler.scale(Visualizer.vv, 1/1.1f, Visualizer.vv.getCenter());
            }
        });
        refresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	Visualizer.changeVisualization( false, Algorithm.code );
            }
        });
        analysis.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	try {
					AnalysisColorer.getChooser();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
        
        Visualizer.changeVisualization( false, Algorithm.ALG_RADIAL );
        
	}
	
	public static Dimension getDimension() {
		return new Dimension(WIDTH-60,HEIGHT-5);
	}
	
	
	
	
	
	
}