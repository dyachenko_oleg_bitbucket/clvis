package app.gui;

import java.util.Vector;

public interface AbstractModelInterface {
	
	public void addRow(Vector<Object> row)  throws Exception ;
	public void clear();
	public void putContents(Vector<Vector<Object>> data);
	public Vector<Vector<Object>> getContents();
}
