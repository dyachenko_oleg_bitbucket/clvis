package app.gui;

import java.awt.Point;
import java.util.Vector;

import javax.swing.JTable;

public class Table extends JTable implements TableInterface {
	private static final long serialVersionUID = 1L;
	
	public Table(AbstractModel am){
		super(am);
	}

	public void addRenderer(int col) {
		setDefaultRenderer(getColumnClass(col), TableRenderer.val);
	}

	public void setColWidth(int col, int width) {
		getColumnModel().getColumn(col).setMaxWidth(width);
		getColumnModel().getColumn(col).setMinWidth(width);
		getColumnModel().getColumn(col).setPreferredWidth(width);
	}

	public void putContents(Vector<Vector<Object>> data) {
		getTableModel().putContents(data);
	}
	
	public void clear() {
		getTableModel().clear();
	}

	public AbstractModel getTableModel() {
		return (AbstractModel)getModel();
	}

	public void addRow(Vector<Object> row) throws Exception {
		getTableModel().addRow(row);
	}

	public Vector<Vector<Object>> getContents() {
		return getTableModel().getContents();
	}	
	
	public Object getValueAt(int rowIndex, int columnIndex) {
		return getModel().getValueAt(rowIndex, columnIndex);
	}
	
	public Point getSelectedCell() {
		Point pt = new Point( getSelectedRow(), getSelectedColumn() );
		if( pt.x >= 0 && pt.y >= 0 ) {
			return pt;
		}
		return null;
	}
	public Object getSelectedValue() {
		Point pt = getSelectedCell();
		if( pt != null ) {
			return getValueAt(pt.x, pt.y);
		}
		return null;
	}
	
}
