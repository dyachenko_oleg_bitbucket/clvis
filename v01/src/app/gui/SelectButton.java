package app.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class SelectButton extends JButton {
	private static final long serialVersionUID = 1L;

	private int state = -1;
	private ArrayList<String> paths = new ArrayList<String>();
	
	public SelectButton( String[] paths ) {
		for( String s : paths )
			this.paths.add(s);
		setState(0);
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nextState();
			}
		});
	}
		
	public int getState() {
		return state;
	}
	public void setState( int state ) {
		if( paths.isEmpty() ) return;
		this.state = state;
		this.state = this.state % paths.size();
		this.setIcon( new ImageIcon(paths.get(this.state)) );
		this.repaint();
	}
	public void nextState() {
		setState( getState()+1 );
	}
	
	
}
