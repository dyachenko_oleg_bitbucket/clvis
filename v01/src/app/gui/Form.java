package app.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class Form extends JFrame implements FormInterface {
	private static final long serialVersionUID = 1L;

	private Form parent = null;
	
	public Form( Form parent ) {
		this.parent = parent;
		gainFocus();
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				loseFocus();
			}
			@Override
			public void windowClosed(WindowEvent e) {
				loseFocus();
			}
		});
	}
	
	protected void loseFocus() {
		if( parent != null ) {
			parent.setVisible(true);
			parent.update();
		}
		else {
			System.exit(0);
		}
	}
	protected void gainFocus() {
		if( parent != null )
			parent.setVisible(false);
	}
	protected void update() {
		
	}
	
	
	
	
	protected void putElementAtGrid(GridBagConstraints gridConstr, int gridwidth, int gridheight, int gridx, int gridy, int width, int height, JComponent obj) {
		gridConstr.gridwidth = gridwidth;
		gridConstr.gridheight = gridheight;
		gridConstr.gridx = gridx;
		gridConstr.gridy = gridy;
		setFixedSize(obj, new Dimension(width, height));
		getContentPane().add(obj, gridConstr);
	}

	private void setFixedSize( JComponent obj, Dimension dim ) {
		obj.setPreferredSize(dim);
        obj.setMinimumSize(dim);
        obj.setMaximumSize(dim);
	}
	
	protected GridBagConstraints getDefaultGridBagConstraints() {
		setLayout(new GridBagLayout());
		GridBagConstraints gridConstr = new GridBagConstraints();
		gridConstr.insets = new Insets(2,2,2,2);
		gridConstr.anchor = GridBagConstraints.NORTH; 
        gridConstr.fill   = GridBagConstraints.NONE;  
        gridConstr.insets = new Insets(2,2,2,2);
        return gridConstr;
	}
	
	public void init(String formName, int width, int height) {
		addComponents();
		setEvents();
		try{
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel");
			SwingUtilities.updateComponentTreeUI(this);
		} catch(Exception e){};
		setLocation(new Point(100, 100));
		setPreferredSize(new Dimension(width, height));
		setMinimumSize(new Dimension(width, height));
		setMaximumSize(new Dimension(width, height));
		setResizable(false);
		setTitle(formName);
		setVisible(true);
		pack();
	}
	
	@Override
	public void addComponents() {}
	@Override
	public void setEvents() {}
	
	protected void hideParent() {
		
	}
	protected void showParent() {
		
	}
	
}
