package app.graph;

import java.util.ArrayList;
import java.util.Vector;


public class Node implements NodeInterface {
	private String id = ""; 
	private String entity = ""; 
	
	public String getId() {
		return id;
	}
	public String setId(String id) {
		this.id = id;
		return this.id;
	}
	public String getEntity() {
		return entity;
	}
	
	
	public ArrayList<Vector<String>> info() {
		return null;
	}
	
	
	public Node( String id, String entity ) {
		this.id = id;
		this.entity = entity;		
	}
}
