package app.config;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import app.xml.CL2XML;



public class Configuration {
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
		
	private static HashMap<String, String> data = new HashMap<String, String>();
	private static boolean initialized = false;
	
	public static String path = "../config.xml";
	
	public static void init() {
		if( initialized ) {
			return;
		}
		getFromXML();
		if( !(new File("../"+get("dataPath"))).exists() ) {
			try {
				CL2XML.createXML("../"+get("dataPath"), true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if( !(new File("../"+get("ontoPath"))).exists() ) {
			try {
				CL2XML.createXML("../"+get("ontoPath"), false);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		initialized = true;
	}
	
	public static void reload() {
		initialized = false;
		init();
	}
	
	public static String get(String key) {
		if( data.containsKey(key) ) {
			return data.get(key);
		}
		return "";
	}
	
	public static String put(String key, String val) {
		return data.put(key, val);
	}
	
	private static void getFromXML() {
		SAXBuilder builder = new SAXBuilder();
		File xmlFile = new File(path);
		if (!xmlFile.exists()) {
			JOptionPane.showMessageDialog(null, "XML config missing at the path "+xmlFile.getAbsolutePath());
		}
		
		try {
			Document document = (Document) builder.build(xmlFile);
			Element rootNode = document.getRootElement();
			
			@SuppressWarnings("rawtypes")
			List list = rootNode.getChildren();
			for( int i=0; i<list.size(); i++) {
				Element node = (Element) list.get(i);
				data.put( node.getName() , node.getValue() );
			}
	 	} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (JDOMException jdomex) {
			System.out.println(jdomex.getMessage());
		}
	}
	
	public static void clean_data() {
		data = new HashMap<String, String>();
	}
	public static void clean() {
		clean_data();
	}
	
	public static void saveToXML() {		
		Element root = new Element("data");
		Document doc = new Document(root);
		doc.setRootElement(root);
		
		for( String key: data.keySet() ) {
			doc.getRootElement().addContent( new Element(key).addContent(data.get(key)) );
		}
		
		XMLOutputter xmlOutput = new XMLOutputter();
		xmlOutput.setFormat(Format.getPrettyFormat());
		try {
			xmlOutput.output(doc, new FileWriter(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
